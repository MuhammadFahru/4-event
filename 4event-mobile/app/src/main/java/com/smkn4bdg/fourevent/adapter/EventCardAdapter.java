package com.smkn4bdg.fourevent.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.smkn4bdg.fourevent.DetailEventActivity;
import com.smkn4bdg.fourevent.R;
import com.smkn4bdg.fourevent.models.Event;

import java.util.ArrayList;

public class EventCardAdapter extends RecyclerView.Adapter<EventCardAdapter.EventCardViewHolder>{
    private ArrayList<Event> dataList;

    public EventCardAdapter(ArrayList<Event> dataList){
        this.dataList = dataList;
    }

    @Override
    public EventCardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.layout_card_event, parent, false);
        return new EventCardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EventCardViewHolder holder, int position) {
        holder.ivEvent.setImageResource(dataList.get(position).getImage());
        holder.txtCategory.setText(dataList.get(position).getCategory());
        holder.txtTitle.setText(dataList.get(position).getTitle());
        holder.txtOnline.setText(dataList.get(position).getOnline());
        holder.txtDate.setText(dataList.get(position).getDate());

    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class EventCardViewHolder extends RecyclerView.ViewHolder {
        private TextView txtTitle, txtCategory, txtOnline, txtDate, txtStatus;
        private ImageView ivEvent;
        CardView cardEvent;
//        private String title, online, date;

        public EventCardViewHolder(final View itemView) {
            super(itemView);
            ivEvent = (ImageView) itemView.findViewById(R.id.iv_event_image);
            txtCategory = (TextView) itemView.findViewById(R.id.txt_event_category);
            txtTitle = (TextView) itemView.findViewById(R.id.txt_event_title);
            txtOnline = (TextView) itemView.findViewById(R.id.txt_event_online);
            txtDate = (TextView) itemView.findViewById(R.id.txt_event_date);
            cardEvent = (CardView) itemView.findViewById(R.id.card_event);

            cardEvent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent( v.getContext(), DetailEventActivity.class);
                    v.getContext().startActivity(i);
                }
            });

        }
    }
}
