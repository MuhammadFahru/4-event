package com.smkn4bdg.fourevent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.widget.LinearLayout;

import com.denzcoskun.imageslider.ImageSlider;
import com.denzcoskun.imageslider.models.SlideModel;
import com.smkn4bdg.fourevent.adapter.BannerAdapter;
import com.smkn4bdg.fourevent.adapter.EventCardAdapter;
import com.smkn4bdg.fourevent.adapter.SliderPagerAdapter;
import com.smkn4bdg.fourevent.decoration.BannerSlider;
import com.smkn4bdg.fourevent.decoration.SliderIndicator;
import com.smkn4bdg.fourevent.fragment.FragmentSlider;
import com.smkn4bdg.fourevent.models.Event;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    private SliderPagerAdapter mAdapter;
    private SliderIndicator mIndicator;

    private BannerSlider bannerSlider;
    private LinearLayout mLinearLayout;

    private RecyclerView rvTrending;
    private EventCardAdapter eventCardAdapter;

    private RecyclerView rvRecommend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewInit();
        trendingRecycler();
        recomendedRecycler();

        ImageSlider imageSlider = findViewById(R.id.home_slider);

        List<SlideModel> slideModels = new ArrayList<>();

        slideModels.add(new SlideModel("https://image.freepik.com/free-vector/music-event-banner-design-template-festival-concert-party_85212-64.jpg", ""));
        slideModels.add(new SlideModel("https://d1csarkz8obe9u.cloudfront.net/posterpreviews/open-mic-night-facebook-event-banner-design-template-5a951f3dcd85d692ef014b7594d11498_screen.jpg?ts=1566599540", ""));

        imageSlider.setImageList(slideModels, true);

//        bannerSlider = findViewById(R.id.sliderView);
//        mLinearLayout = findViewById(R.id.pagesContainer);
//        setupSlider();
    }
    private void findViewInit(){
        rvTrending = findViewById(R.id.home_trending);
        rvRecommend = findViewById(R.id.home_recommended);
    }
//    private void setupSlider() {
//        bannerSlider.setDurationScroll(800);
//        List<Fragment> fragments = new ArrayList<>();
//
//        //link image
//        fragments.add(FragmentSlider.newInstance("https://ecs7.tokopedia.net/img/banner/2020/4/19/85531617/85531617_17b56894-2608-4509-a4f4-ad86aa5d3b62.jpg"));
//        fragments.add(FragmentSlider.newInstance("https://ecs7.tokopedia.net/img/banner/2020/4/19/85531617/85531617_7da28e4c-a14f-4c10-8fec-845578f7d748.jpg"));
//        fragments.add(FragmentSlider.newInstance("https://ecs7.tokopedia.net/img/banner/2020/4/18/85531617/85531617_87a351f9-b040-4d90-99f4-3f3e846cd7ef.jpg"));
//        fragments.add(FragmentSlider.newInstance("https://ecs7.tokopedia.net/img/banner/2020/4/20/85531617/85531617_03e76141-3faf-45b3-8bcd-fc0962836db3.jpg"));
//
//        mAdapter = new SliderPagerAdapter(getSupportFragmentManager(), fragments);
//        bannerSlider.setAdapter(mAdapter);
//        mIndicator = new SliderIndicator(this, mLinearLayout, bannerSlider, R.drawable.indicator_circle);
//        mIndicator.setPageCount(fragments.size());
//        mIndicator.show();
//    }

    private void trendingRecycler() {

        rvTrending.setHasFixedSize(true);
        rvTrending.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        ArrayList<Event> event = new ArrayList<>();
        event.add(new Event(R.drawable.img1, "Webminar", "Belajar Android", "Online", "27 Maret 2021"));
        event.add(new Event(R.drawable.img2, "Beasiswa", "Tips Masuk Univ", "Online", "1 Maret 2021"));


        eventCardAdapter = new EventCardAdapter(event);
        rvTrending.setAdapter(eventCardAdapter);

    }

    private void recomendedRecycler() {

        rvRecommend.setHasFixedSize(true);
        rvRecommend.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        ArrayList<Event> event = new ArrayList<>();
        event.add(new Event(R.drawable.img1, "Webminar", "Belajar Android", "Online", "27 Maret 2021"));
        event.add(new Event(R.drawable.img2, "Beasiswa", "Tips Masuk Univ", "Online", "1 Maret 2021"));


        eventCardAdapter = new EventCardAdapter(event);
        rvRecommend.setAdapter(eventCardAdapter);

    }

}