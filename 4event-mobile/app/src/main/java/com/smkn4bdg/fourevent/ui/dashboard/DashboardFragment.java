package com.smkn4bdg.fourevent.ui.dashboard;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.smkn4bdg.fourevent.R;
import com.smkn4bdg.fourevent.adapter.EventCardAdapter;
import com.smkn4bdg.fourevent.models.Event;

import java.util.ArrayList;

public class DashboardFragment extends Fragment {

    private DashboardViewModel dashboardViewModel;
    private RecyclerView rvEventList;
    private EventCardAdapter eventCardAdapter;

    View root;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel =new ViewModelProvider(this).get(DashboardViewModel.class);
        root = inflater.inflate(R.layout.fragment_dashboard, container, false);

        findViewInit();
        eventRecycler();

        return root;
    }

    private void findViewInit(){
        rvEventList = root.findViewById(R.id.event_list);
    }

    private void eventRecycler() {

        rvEventList.setHasFixedSize(true);
        rvEventList.setLayoutManager(new GridLayoutManager(root.getContext(), 2));

        ArrayList<Event> event = new ArrayList<>();
        event.add(new Event(R.drawable.img1, "Webminar", "Belajar Android", "Online", "27 Maret 2021"));
        event.add(new Event(R.drawable.img2, "Beasiswa", "Tips Masuk Univ", "Online", "1 Maret 2021"));
        event.add(new Event(R.drawable.img1, "Webminar", "Belajar Android", "Online", "27 Maret 2021"));
        event.add(new Event(R.drawable.img2, "Beasiswa", "Tips Masuk Univ", "Online", "1 Maret 2021"));
        event.add(new Event(R.drawable.img1, "Webminar", "Belajar Android", "Online", "27 Maret 2021"));
        event.add(new Event(R.drawable.img2, "Beasiswa", "Tips Masuk Univ", "Online", "1 Maret 2021"));

        eventCardAdapter = new EventCardAdapter(event);
        rvEventList.setAdapter(eventCardAdapter);

    }

}