package com.smkn4bdg.fourevent.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.denzcoskun.imageslider.ImageSlider;
import com.denzcoskun.imageslider.models.SlideModel;
import com.smkn4bdg.fourevent.R;
import com.smkn4bdg.fourevent.adapter.EventCardAdapter;
import com.smkn4bdg.fourevent.models.Event;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private RecyclerView rvTrending;
    private EventCardAdapter eventCardAdapter;

    private RecyclerView rvRecommend;

    View root;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        root = inflater.inflate(R.layout.fragment_home, container, false);
        findViewInit();
        trendingRecycler();
        recomendedRecycler();

        ImageSlider imageSlider = root.findViewById(R.id.home_slider);

        List<SlideModel> slideModels = new ArrayList<>();

        slideModels.add(new SlideModel("https://image.freepik.com/free-vector/music-event-banner-design-template-festival-concert-party_85212-64.jpg", ""));
        slideModels.add(new SlideModel("https://d1csarkz8obe9u.cloudfront.net/posterpreviews/open-mic-night-facebook-event-banner-design-template-5a951f3dcd85d692ef014b7594d11498_screen.jpg?ts=1566599540", ""));

        imageSlider.setImageList(slideModels, true);
        return root;
    }

    private void findViewInit(){
        rvTrending = root.findViewById(R.id.home_trending);
        rvRecommend = root.findViewById(R.id.home_recommended);
    }

    private void trendingRecycler() {

        rvTrending.setHasFixedSize(true);
        rvTrending.setLayoutManager(new LinearLayoutManager(root.getContext(), LinearLayoutManager.HORIZONTAL, false));

        ArrayList<Event> event = new ArrayList<>();
        event.add(new Event(R.drawable.img1, "Webminar", "Belajar Android", "Online", "27 Maret 2021"));
        event.add(new Event(R.drawable.img2, "Beasiswa", "Tips Masuk Univ", "Online", "1 Maret 2021"));

        eventCardAdapter = new EventCardAdapter(event);
        rvTrending.setAdapter(eventCardAdapter);

    }

    private void recomendedRecycler() {

        rvRecommend.setHasFixedSize(true);
        rvRecommend.setLayoutManager(new LinearLayoutManager(root.getContext(), LinearLayoutManager.HORIZONTAL, false));

        ArrayList<Event> event = new ArrayList<>();
        event.add(new Event(R.drawable.img1, "Webminar", "Belajar Android", "Online", "27 Maret 2021"));
        event.add(new Event(R.drawable.img2, "Beasiswa", "Tips Masuk Univ", "Online", "1 Maret 2021"));

        eventCardAdapter = new EventCardAdapter(event);
        rvRecommend.setAdapter(eventCardAdapter);

    }
}