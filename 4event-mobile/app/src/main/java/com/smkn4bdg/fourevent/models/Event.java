package com.smkn4bdg.fourevent.models;

public class Event {
    private int image;
    private String category;
    private String title;
    private String online;
    private String date;


    public Event(int image, String category, String title, String online, String date) {
        this.setImage(image);
        this.setTitle(title);
        this.setCategory(category);
        this.setTitle(title);
        this.setOnline(online);
        this.setDate(date);
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
