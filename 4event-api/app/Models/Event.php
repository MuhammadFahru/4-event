<?php

namespace App\Models;

use App\Models\User;
use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    public $table = 'events';

    protected $guarded = [
        'id'
    ];

    protected $dates = ['tanggal_mulai', 'tanggal_selesai', 'deadline_pendaftaran'];

    public function user(){
        return $this->belongsTo(User::class, 'id_user', 'id');
    }

    public function category(){
        return $this->belongsTo(Category::class, 'id_kategori', 'id');
    }
}
