<?php
namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Participant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventController extends Controller
{
    public function index(){
        $event = Event::with('user', 'category')
        ->get();

        return response()->json($event);
    }

    //detail event
    public function show($id)
    {
        $event = Event::with('user', 'category')
        ->find($id);
        return response()->json($event);
    }

    // event terbaru di home
    public function latest()
    {
        $event = Event::latest()
        ->limit(5)
        ->get();
        return response()->json($event);
    }

    public function recommended()
    {

    }

    //untuk banner
    public function highlight()
    {
        $event = Event::latest()
        ->limit(5)
        ->get();
        return response()->json($event);
    }

    public function register(Request $request){
        $participant = new Participant();
        $participant->id_event = $request->id_event;
        $participant->id_user = $request->id_user;
        $participant->id_bidang = $request->id_bidang;
        $participant->id_kelas = $request->id_kelas;
        $participant->data_pendaftar = $request->data_pendaftar;
        $participant->dokumen_pendaftaran = $request->dokumen_pendaftaran;

        $status = $participant->save();

        if($status){
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status' => 'error']);
        }
    }

}