<?php
namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Event;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventCategoryController extends Controller
{
    public function index(){
        $category = Category::get();

        return response()->json($category);
    }

    
    //show event by category
    public function show($id)
     {
        $event = Event::where('id_kategori', '=', $id)
        ->with('user', 'category')
        ->get();

        return response()->json($event);
     }
}