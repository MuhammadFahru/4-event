<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// $router->get('/', function () use ($router) {
//     return $router->app->version();
// });



$router->group(['prefix' => 'api/v1'], function () use ($router) {

    $router->get('/events', 'EventController@index');
    $router->get('/event/categories', 'EventCategoryController@index');
    $router->get('/event/latest', 'EventController@latest');
    $router->get('/event/highlight', 'EventController@highlight');


    //auth
    $router->post('/login', 'AuthController@login');

    $router->post('/event/register', 'EventController@register');


    $router->get('/event/{id}', 'EventController@show');
    $router->get('/event/category/{id}', 'EventCategoryController@show');

 });



