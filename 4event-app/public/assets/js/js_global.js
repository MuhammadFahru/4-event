function set_validator_input(arr_input, response) {
    var no = 1;
    arr_input.forEach((element, index) => {
        if (typeof response.message[element] === 'undefined') {
            $("#" + element).removeClass('is-invalid')
            $("#feedback_" + element).removeClass('invalid-feedback')
            $("#feedback_" + element).text('')
        } else {
            if (no == 1) {
                setTimeout(() => { $("#"+element).focus() }, 500);
            }
            no++     
            $("#" + element).addClass('is-invalid')
            $("#feedback_" + element).addClass('invalid-feedback')
            $("#feedback_" + element).text(response.message[element][0])
        }
    });
}

function set_validator_reset() {
    $(".reset_input").removeClass('is-invalid')
    $(".reset_feedback").removeClass('invalid-feedback')
    $(".reset_feedback").text('')
}

function set_single_select2(name_id, text, allow_clear=true, data_arr="", width_size='100%') {
    $('#'+name_id).select2({
        placeholder: text,
        allowClear: allow_clear,
        theme: "bootstrap",
        data: data_arr,
        width: width_size,
    });
}

function set_badge_status (data) {
    badge = ``;
    if(data == 1) {
        badge += `<h5><span class="badge badge-success p-2">Official</span></h5>`;
    } else {
        badge += `<h5><span class="badge badge-secondary p-2">Non Official</span></h5>`;
    }
    return badge;
}

function set_layout () {
    layout = ``;
}

function buttonActive() {
    if (document.getElementById("konfirmasi-btn").checked) {
        var element = document.getElementById("next");
        element.classList.remove("disabled");
    } else {
        var element = document.getElementById("next");
        element.classList.add("disabled");
    }           
}

function changePage() {
    if (document.getElementById("konfirmasi-btn").checked) {
        document.getElementById("konfirmasi-tab").classList.remove("active");
        document.getElementById("konfirmasi").classList.remove("active");
        document.getElementById("konfirmasi").classList.remove("show");
        document.getElementById("input-tab").classList.add("active");
        document.getElementById("input").classList.add("active");
        document.getElementById("input").classList.add("show");
        document.getElementById("card-section").classList.remove("bg-dark");               
    } else {
        
    }
}

function getComboA(selectObject) {
    var value = selectObject.value;
    if (value == "Online") {
        console.log(value);
        document.getElementById("lokasi").setAttribute("disabled", "");
        document.getElementById("lokasi-section").classList.add("d-none");
        document.getElementById("lokasi").value("Event Online");
    } else if (value == "Offline") {
        console.log(value);
        document.getElementById("lokasi").removeAttribute("disabled");        
        document.getElementById("lokasi-section").classList.remove("d-none");
    }
}