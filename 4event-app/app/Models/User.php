<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    const ROLE_SUPER_ADMIN = 1;
    const ROLE_OSIS = 2;
    const ROLE_EO = 3;
    const ROLE_GURU_STAFF = 4;
    const ROLE_SISWA = 5;
    const ROLE_UMUM = 6;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getRoleName()
    {
        if ($this->role_id == self::ROLE_SUPER_ADMIN) {
            return "Super Admin";
        }
        if ($this->role_id == self::ROLE_OSIS) {
            return "OSIS";
        }
        if ($this->role_id == self::ROLE_EO) {
            return "EO";
        }
        if ($this->role_id == self::ROLE_GURU_STAFF) {
            return "Guru dan Staff";
        }
        if ($this->role_id == self::ROLE_SISWA) {
            return "siswa";
        }
        if ($this->role_id == self::ROLE_UMUM) {
            return "Umum";
        }
    }

    public static function rolename($role_id)
    {
        if ($role_id == self::ROLE_SUPER_ADMIN) {
            return "Super Admin";
        }
        if ($role_id == self::ROLE_OSIS) {
            return "OSIS";
        }
        if ($role_id == self::ROLE_EO) {
            return "EO";
        }
        if ($role_id == self::ROLE_GURU_STAFF) {
            return "Guru dan Staff";
        }
        if ($role_id == self::ROLE_SISWA) {
            return "siswa";
        }
        if ($role_id == self::ROLE_UMUM) {
            return "Umum";
        }
    }
}
