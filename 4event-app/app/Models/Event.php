<?php

namespace App\Models;

use App\Models\Field;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    public $table = 'events';

    protected $guarded = [
        'id'
    ];

    protected $dates = ['tanggal_mulai', 'tanggal_selesai', 'deadline_pendaftaran'];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function bidang(){
        return $this->hasMany(Field::class, 'id_event', 'id');
    }
}
