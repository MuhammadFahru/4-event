<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string $role
     * @return mixed
     */
    public function handle($request, Closure $next, ...$role)
    {
        foreach ($role as $value) {
            if (Auth::user()->role_id == $value) {
                return $next($request);
            }
        }
        return redirect(RouteServiceProvider::HOME);
    }
}
