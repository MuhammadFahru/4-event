<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Event;
use App\Models\Field;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class EventController extends Controller
{
    public function index()
    {
        $data['event'] = Event::join('categories as b', 'b.id', 'events.id_kategori')
            ->join('users as c', 'c.id', 'events.id_user')
            ->selectRaw('events.*, b.nama_kategori, c.foto, c.name')
            ->latest();
        if (Auth::user()->role_id != 1) {
            $data['event'] = $data['event']->where('id_user', Auth::user()->id);
        }
        $data['event'] = $data['event']->paginate(6);
        return view('admin.event.index', $data);
    }

    public function indexAjax(Request $request)
    {
        $db = Event::join('categories as b', 'b.id', 'events.id_kategori')
            ->join('users as c', 'c.id', 'events.id_user')
            ->selectRaw('events.*, b.nama_kategori, c.foto, c.name');
        $data = $db->get();
        return datatables()->of($data)->addIndexColumn()->toJson();
    }

    public function create()
    {
        $data['kategori'] = Category::get();
        return view('admin.event.form', $data);
    }

    public function verifikasiData(Request $request)
    {
        $this->validate($request, [
            'foto' => 'required|image|mimes:jpeg,png,jpg,jfif|max:1024',
            'jenis_kelamin' => 'required',
            'tanggal_lahir' => 'required',
            'tempat_lahir' => 'required',
            'tempat_tinggal' => 'required',
            'bukti_identitas' => 'required|image|mimes:jpeg,png,jpg,jfif|max:1024'
        ]);

        $file1 = $request->file('foto')->store('foto-profil', 'public');
        $file2 = $request->file('bukti_identitas')->store('bukti-identitas', 'public');

        $data = User::findOrFail(Auth::user()->id);
        $data->foto = $file1;
        $data->jenis_kelamin = $request->jenis_kelamin;
        $data->tanggal_lahir = $request->tanggal_lahir;
        $data->tempat_lahir = $request->tempat_lahir;
        $data->tempat_tinggal = $request->tempat_tinggal;
        $data->bukti_identitas = $file2;
        $data->status_identitas = 1;
        $data->role_id = 3;
        $status = $data->save();

        if ($status) {
            return redirect()->route('event.create')->with('success', 'Verifikasi Data Berhasil');
        } else {
            return redirect()->route('event.create')->with('error', 'Verifikasi Data Tidak Berhasil');
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'banner' => 'required|image|mimes:jpeg,png,jpg,jfif|max:1024',
            'nama_event' => 'required',
            'kategori_event' => 'required',
            'tanggal_mulai' => 'required',
            'tanggal_selesai' => 'required',
            'jenis_pendaftar' => 'required',
            'deadline_pendaftaran' => 'required',
            'link' => 'required',
            'pelaksanaan' => 'required',
            'deskripsi' => 'required',
            'panduan_pendaftaran' => 'mimes:jpeg,png,jpg,jfif,pdf|max:1024'
        ]);

        $banner = $request->file('banner')->store('banner-event', 'public');
        if ($request->hasfile('panduan_pendaftaran')) {
            $panduan_pendaftaran = $request->file('panduan_pendaftaran')->store('panduan_pendaftaran', 'public');
        }

        if (!empty($request->dokumen_pendaftaran)) {
            $dokumen_pendaftaran = 1;
        } else {
            $dokumen_pendaftaran = 0;
        }

        if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2) {
            $status_event = "Official";
        } else {
            $status_event = "Unofficial";
        }

        $event = new Event();
        $event->id_user = Auth::user()->id;
        $event->id_kategori = $request->kategori_event;
        $event->status_event = $status_event;
        $event->banner = $banner;
        $event->nama_event = $request->nama_event;
        $event->slug = Str::slug($request->nama_event) . '-' . Str::random(5);
        $event->tanggal_mulai = $request->tanggal_mulai;
        $event->tanggal_selesai = $request->tanggal_selesai;
        $event->jenis_pendaftar = $request->jenis_pendaftar;
        $event->deadline_pendaftaran = $request->deadline_pendaftaran;
        $event->link = $request->link;
        $event->pelaksanaan = $request->pelaksanaan;
        $event->deskripsi = $request->deskripsi;
        $event->dokumen_pendaftaran = $dokumen_pendaftaran;
        if (!empty($request->lokasi)) {
            $event->lokasi = $request->lokasi;
        } else {
            $event->lokasi = "Event Online";
        }
        if ($request->hasfile('panduan_pendaftaran')) {
            $event->panduan_pendaftaran = $panduan_pendaftaran;
        }
        $event->save();

        if (!empty($request->bidang)) {
            if (sizeof($request->bidang) > 1 && $request->bidang[0] != null) {
                $id_event_arr = $request->bidang;
                foreach ($id_event_arr as $value) {
                    $bidang = new Field();
                    $bidang->id_event = $event->id;
                    $bidang->nama_bidang = $value;
                    $bidang->save();
                }
                if ($event && $bidang) {
                    return redirect()->route('event.index')->with('success', 'Event Berhasil Diupload');
                } else {
                    return redirect()->route('event.index')->with('error', 'Event Gagal Diupload');
                }
            }
        }

        if ($event) {
            return redirect()->route('event.index')->with('success', 'Event Berhasil Diupload');
        } else {
            return redirect()->route('event.index')->with('error', 'Event Gagal Diupload');
        }
    }

    public function edit(Event $event)
    {
        $kategori = Category::get();
        $bidang = Field::where('id_event', $event->id)->latest()->get();
        return view('admin.event.form', compact('event', 'kategori', 'bidang'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'banner' => 'image|mimes:jpeg,png,jpg,jfif|max:1024',
            'nama_event' => 'required',
            'kategori_event' => 'required',
            'tanggal_mulai' => 'required',
            'tanggal_selesai' => 'required',
            'jenis_pendaftar' => 'required',
            'deadline_pendaftaran' => 'required',
            'link' => 'required',
            'pelaksanaan' => 'required',
            'deskripsi' => 'required',
            'panduan_pendaftaran' => 'mimes:jpeg,png,jpg,jfif,pdf|max:1024'
        ]);

        if ($request->hasfile('banner')) {
            $banner = $request->file('banner')->store('banner-event', 'public');
        }

        if ($request->hasfile('panduan_pendaftaran')) {
            $panduan_pendaftaran = $request->file('panduan_pendaftaran')->store('panduan_pendaftaran', 'public');
        }

        $event = Event::findOrFail($id);
        $event->id_kategori = $request->kategori_event;
        $event->nama_event = $request->nama_event;
        $event->tanggal_mulai = $request->tanggal_mulai;
        $event->tanggal_selesai = $request->tanggal_selesai;
        $event->jenis_pendaftar = $request->jenis_pendaftar;
        $event->deadline_pendaftaran = $request->deadline_pendaftaran;
        $event->link = $request->link;
        $event->pelaksanaan = $request->pelaksanaan;
        $event->deskripsi = $request->deskripsi;
        if (!empty($request->lokasi)) {
            $event->lokasi = $request->lokasi;
        } else {
            $event->lokasi = "Event Online";
        }
        if ($request->hasfile('banner')) {
            $event->banner = $banner;
        }
        if ($request->hasfile('panduan_pendaftaran')) {
            $event->panduan_pendaftaran = $panduan_pendaftaran;
        }
        $status = $event->save();

        if (!empty($request->bidang)) {
            if (!empty($request->id_bidang)) {
                $event_arr = $request->bidang;
                $id_event = $request->id_bidang;

                foreach (array_combine($event_arr, $id_event) as $nama => $id) {
                    if (is_null($id)) {
                        $bidang = new Field();
                        $bidang->id_event = $event->id;
                        $bidang->nama_bidang = $nama;
                        $bidang->save();
                    } else {
                        $bidang = Field::findOrFail($id);
                        $bidang->id_event = $event->id;
                        $bidang->nama_bidang = $nama;
                        $bidang->save();
                    }
                }
            } else {
                $id_event_arr = $request->bidang;
                foreach ($id_event_arr as $value) {
                    $bidang = new Field();
                    $bidang->id_event = $event->id;
                    $bidang->nama_bidang = $value;
                    $bidang->save();
                }
            }
        }

        if ($status) {
            return redirect()->route('event.index')->with('success', 'Event Berhasil Diupdate');
        } else {
            return redirect()->route('event.index')->with('error', 'Event Gagal Diupdate');
        }
    }

    public function deleteBidang($id)
    {
        Field::findOrFail($id)->delete();
        return redirect()->back()->with('success', "Bidang Event Berhasil Dihapus");
    }

    public function destroy($id)
    {
        Event::findOrFail($id)->delete();
        return redirect()->route('event.index')->with('success', "Event Berhasil Dihapus");
    }

    public function search()
    {
        $search = request('search');
        $pelaksanaan = request('pelaksanaan_search');
        $status = request('status_search');
        $event = Event::join('categories as b', 'b.id', 'events.id_kategori')
            ->join('users as c', 'c.id', 'events.id_user')
            ->selectRaw('events.*, b.nama_kategori, c.foto, c.name');

        if (Auth::user()->role_id != 1) {
            $event = $event->where('id_user', Auth::user()->role_id);
        }

        if ($pelaksanaan != null) {
            $event = $event->where('pelaksanaan', $pelaksanaan);
        }

        if ($status != null) {
            $event = $event->where('status_event', $status);
        }

        if ($search != null) {
            $event = $event->where('nama_event', 'LIKE', "%$search%")
                ->orWhere('pelaksanaan', 'LIKE', "%$search%")
                ->orWhere('nama_kategori', 'LIKE', "%$search%");
        }

        $event = $event->latest()->paginate(6);

        return view('admin.event.index', compact('event', 'search', 'pelaksanaan', 'status'));
    }

    public function show(Event $event)
    {
        $data['event'] = Event::join('categories as a', 'a.id', 'events.id_kategori')
            ->join('users as b', 'b.id', 'events.id_user')
            ->where('events.id', $event->id)
            ->selectRaw('events.*, a.nama_kategori, b.foto, b.name')
            ->first();
        return view('admin.event.detail', $data);
    }
}
