<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Category;
use Validator;
use DB;
use DataTables;

class KategoriController extends Controller
{
    public function index()
    {
        return view('admin.kategori.index');
    }

    public function indexAjax()
    {
        $db = Category::selectRaw("categories.*")->orderBy('categories.id', 'ASC');
        $data = $db->get();
        return datatables()->of($data)->addIndexColumn()->toJson();
    }

    public function getKategori(Request $request)
    {
        try {
            $kategori = Category::selectRaw("categories.*")
                ->where('categories.id', $request->id)
                ->first();

            return response()->json([
                'status' => 'success',
                'data' => $kategori,
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'status' => 'error',
                'icon' => 'error',
                'message' => $th->getMessage(),
            ]);
        }
    }

    public function store(Request $request)
    {
        $rules = [
            'nama_kategori' => 'required|string|unique:categories,nama_kategori'
        ];

        $validator  = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'validasi',
                'message' => $validator->errors()
            ]);
        }

        try {
            $kategori = new Category;
            $kategori->nama_kategori = $request->nama_kategori;
            $kategori->save();

            return response()->json([
                'status' => 'success',
                'icon' => 'success',
                'title' => 'Membuat Data Kategori',
                'message' => 'Data Kategori Baru Berhasil Dibuat',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'status' => 'error',
                'icon' => 'error',
                'title' => 'Membuat Data Kategori',
                'message' => $th->getMessage(),
            ]);
        }
    }

    public function update(Request $request)
    {
        $rules = [
            'nama_kategori' => 'required|string|unique:categories,nama_kategori,' . $request->id_kategori . '',
        ];

        $validator  = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'validasi',
                'message' => $validator->errors()
            ]);
        }

        try {
            $kategori = Category::findOrFail($request->id_kategori);
            $kategori->nama_kategori = $request->nama_kategori;
            $kategori->save();

            return response()->json([
                'status' => 'success',
                'icon' => 'success',
                'title' => 'Update Kategori',
                'message' => 'Kategori Berhasil Diupdate',
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'error',
                'icon' => 'error',
                'title' => 'Update Kategori',
                'message' => 'Kategori Gagal diupdate',
            ]);
        }
    }

    public function destroy(Request $request)
    {
        try {
            Category::findOrFail($request->id)->delete();

            return response()->json([
                'status' => 'success',
                'icon' => 'success',
                'title' => 'Hapus Kategori',
                'message' => 'Kategori Berhasil Dihapus',
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'icon' => 'error',
                'title' => 'Hapus Kategori',
                'message' => 'Kategori Gagal Dihapus',
            ]);
        }
    }
}
