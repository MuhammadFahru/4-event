<?php

namespace App\Http\Controllers;

use App\Http\Requests\ValidationUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Testing\Fluent\Concerns\Has;

class UserController extends Controller
{
    public function index()
    {
        $data['user'] = User::get();
        return view('admin.user.index', $data);
    }

    public function create()
    {
        return view('admin.user.form');
    }

    public function store(ValidationUser $request)
    {
        $attr = $request->all();
        $attr['password'] = Hash::make($request->password);
        $attr['salt'] = $request->password;
        $attr['is_active'] = 1;
        $user = User::create($attr);

        if ($user) {
            return redirect()->route('users.index')->with('success', 'Data Berhasil Ditambahkan');
        } else {
            return redirect()->route('users.index')->with('error', 'Data Gagal Ditambahkan');
        }
    }

    public function edit($id)
    {
        $data['user'] = User::findOrFail($id);
        return view('admin.user.form', $data);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => "required|unique:users,email,$id",
            'no_telepon' => "required|unique:users,no_telepon,$id",
            'username' => "required|unique:users,username,$id",
            'role_id' => 'required'
        ]);

        $user = User::findOrFail($id);
        $user->username = $request->username;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->no_telepon = $request->no_telepon;
        $user->role_id = $request->role_id;
        if ($request->password) {
            $user->password = Hash::make($request->password);
            $user->salt = $request->password;
        }
        if ($request->is_active == null) {
            $is_active = 0;
        } else {
            $is_active = 1;
        }
        $user->is_active = $is_active;
        $user->save();

        if ($user) {
            return redirect()->route('users.index')->with('success', 'Data Berhasil Diubah');
        } else {
            return redirect()->route('users.index')->with('error', 'Data Gagal Diubah');
        }
    }

    public function destroy($id)
    {
        User::findOrFail($id)->delete();
        return redirect()->route('users.index')->with('success', "Data Berhasil Dihapus !");
    }
}
