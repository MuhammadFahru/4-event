<?php

namespace App\Http\Controllers\User;

use App\Models\Event;
use App\Models\Kelas;
use App\Models\Participant;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class AllEventController extends Controller
{
    public function index()
    {
        $data['event'] = Event::join('categories as b', 'b.id', 'events.id_kategori')
            ->join('users as c', 'c.id', 'events.id_user')
            ->selectRaw('events.*, b.nama_kategori, c.foto, c.name')
            ->latest();
        $data['event'] = $data['event']->paginate(6);
        return view('user.event.index', $data);
    }

    public function show(Event $event)
    {
        $data['event'] = Event::join('categories as a', 'a.id', 'events.id_kategori')
            ->join('users as b', 'b.id', 'events.id_user')
            ->where('events.id', $event->id)
            ->selectRaw('events.*, a.nama_kategori, b.foto, b.name')
            ->with('bidang')
            ->first();
        return view('user.event.detail', $data);
    }

    public function search()
    {
        $search = request('search');
        $pelaksanaan = request('pelaksanaan_search');
        $status = request('status_search');
        $event = Event::join('categories as b', 'b.id', 'events.id_kategori')
            ->join('users as c', 'c.id', 'events.id_user')
            ->selectRaw('events.*, b.nama_kategori, c.foto, c.name');

        if ($pelaksanaan != null) {
            $event = $event->where('pelaksanaan', $pelaksanaan);
        }

        if ($status != null) {
            $event = $event->where('status_event', $status);
        }

        if ($search != null) {
            $event = $event->where('nama_event', 'LIKE', "%$search%")
                ->orWhere('pelaksanaan', 'LIKE', "%$search%")
                ->orWhere('nama_kategori', 'LIKE', "%$search%");
        }

        $event = $event->latest()->paginate(6);

        return view('user.event.index', compact('event', 'search', 'pelaksanaan', 'status'));
    }

    public function form(Event $event){
        $event = Event::join('categories as a', 'a.id', 'events.id_kategori')
            ->join('users as b', 'b.id', 'events.id_user')
            ->where('events.id', $event->id)
            ->selectRaw('events.*, a.nama_kategori, b.foto, b.name')
            ->with('bidang')
            ->first();
        $kelas = Kelas::get();

        return view('user.event.register', compact('event', 'kelas'));
    }
    public function register(Request $request, Event $event){
        $rule = [
            'bidang' => 'required',
            'kelas' => 'required',
            'data_pendaftar' => 'nullable|string',
        ];

        $messages = [
            'bidang.required' =>'Bidang tidak boleh kosong!',
            'kelas.required' =>'Kelas tidak boleh kosong!',
        ];

        $this->validate($request, $rule, $messages);
        $participant = new Participant;
        $participant->id_event = $event->id;
        $participant->id_user = Auth::user()->id;
        $participant->id_bidang = $request->bidang;
        $participant->id_kelas = $request->kelas;
        $participant->data_pendaftar = $request->data_pendaftar;
        $participant->dokumen_pendaftaran = "-";
        $participant->save();

        if ($participant) {
            return redirect()->route('events')->with('success', 'Pendaftaran Berhasil');
        } else {
            return redirect()->route('events')->with('error', 'Pendaftaran Gagal');
        }

    }


}
