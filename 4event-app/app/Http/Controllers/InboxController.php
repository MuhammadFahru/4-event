<?php

namespace App\Http\Controllers;

use App\Http\Requests\ValidationInbox;
use App\Models\Inbox;
use Illuminate\Http\Request;

class InboxController extends Controller
{
    public function index()
    {
        $data['inbox'] = Inbox::latest()->get();
        return view('admin.pesan.index', $data);
    }

    public function store(ValidationInbox $request)
    {
        $attr = $request->all();
        $inbox = Inbox::create($attr);
        if ($inbox) {
            return redirect('/')->with('success', 'Terima Kasih Sudah Mengirim Pesan');
        } else {
            return redirect('/')->with('error', 'Pesan Tidak Berhasil Dikirim');
        }
    }

    public function detail($id)
    {
        $data['inbox'] = Inbox::findOrFail($id);
        return view('admin.pesan.detail', $data);
    }

    public function search()
    {
        $search = request('search');
        $inbox = Inbox::where('name', 'LIKE', "%$search%")->orWhere('subject', 'LIKE', "%$search%")->orWhere('description', 'LIKE', "%$search%")->get();
        return view('admin.pesan.index', compact('inbox', 'search'));
    }

    public function destroy($id)
    {
        $inbox = Inbox::findOrFail($id)->delete();
        if ($inbox) {
            return redirect()->route('inbox.index')->with('success', 'Pesan Berhasil Dihapus');
        } else {
            return redirect()->route('inbox.index')->with('error', 'Pesan Gagal Dihapus');
        }
    }
}
