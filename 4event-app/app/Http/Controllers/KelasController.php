<?php

namespace App\Http\Controllers;

use App\Models\Jurusan;
use App\Models\Kelas;
use Illuminate\Http\Request;
use Validator;
use DB;
use DataTables;

class KelasController extends Controller
{
    public function index()
    {
        $data['jurusan'] = Jurusan::get();
        return view('admin.kelas.index', $data);
    }

    public function indexAjax(Request $request)
    {
        $db = Kelas::join('jurusan as b', 'class.id_jurusan', 'b.id')
            ->selectRaw("class.*, b.nama_jurusan")
            ->orderBy('class.id', 'ASC');

        if ($request->jenis_jurusan) {
            $db = $db->where('class.id_jurusan', $request->jenis_jurusan);
        }

        $data = $db->get();
        return datatables()->of($data)->addIndexColumn()->toJson();
    }

    public function getKelas(Request $request)
    {
        try {
            $kelas = Kelas::selectRaw("class.*")
                ->where('class.id', $request->id)
                ->first();

            return response()->json([
                'status' => 'success',
                'data' => $kelas,
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'status' => 'error',
                'icon' => 'error',
                'message' => $th->getMessage(),
            ]);
        }
    }

    public function store(Request $request)
    {
        $rules = [
            'nama_kelas' => 'required|string|unique:class,nama_kelas',
            'id_jurusan' => 'required|integer'
        ];

        $validator  = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'validasi',
                'message' => $validator->errors()
            ]);
        }

        try {
            $kelas = new Kelas;
            $kelas->nama_kelas = $request->nama_kelas;
            $kelas->id_jurusan = $request->id_jurusan;
            $kelas->save();

            return response()->json([
                'status' => 'success',
                'icon' => 'success',
                'title' => 'Membuat Data Kelas',
                'message' => 'Data Kelas Baru Berhasil Dibuat',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'status' => 'error',
                'icon' => 'error',
                'title' => 'Membuat Data Kelas',
                'message' => $th->getMessage(),
            ]);
        }
    }

    public function update(Request $request)
    {
        $rules = [
            'nama_kelas' => 'required|string|unique:class,nama_kelas,' . $request->id_kelas . '',
            'id_jurusan' => 'required|integer'
        ];

        $validator  = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'validasi',
                'message' => $validator->errors()
            ]);
        }

        try {
            $kelas = Kelas::findOrFail($request->id_kelas);
            $kelas->nama_kelas = $request->nama_kelas;
            $kelas->id_jurusan = $request->id_jurusan;
            $kelas->save();

            return response()->json([
                'status' => 'success',
                'icon' => 'success',
                'title' => 'Update Kelas',
                'message' => 'Kelas Berhasil Diupdate',
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'error',
                'icon' => 'error',
                'title' => 'Update Kelas',
                'message' => 'Kelas Gagal diupdate',
            ]);
        }
    }

    public function destroy(Request $request)
    {
        try {
            Kelas::findOrFail($request->id)->delete();

            return response()->json([
                'status' => 'success',
                'icon' => 'success',
                'title' => 'Hapus Kelas',
                'message' => 'Kelas Berhasil Dihapus',
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'icon' => 'error',
                'title' => 'Hapus Kelas',
                'message' => 'Kelas Gagal Dihapus',
            ]);
        }
    }
}
