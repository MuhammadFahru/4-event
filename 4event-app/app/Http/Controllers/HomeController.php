<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Event;
use App\Models\Inbox;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::user()->role_id != 1) {
            $data['event'] = Event::where('id_user', Auth::user()->id)->get();
        } else {
            $data['event'] = Event::get();
        }
        $data['kategori'] = Category::get();
        $data['user'] = User::get();
        $data['pesan'] = Inbox::get();
        return view('admin.home', $data);
    }
}
