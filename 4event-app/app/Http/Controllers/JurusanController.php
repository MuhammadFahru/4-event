<?php

namespace App\Http\Controllers;

use App\Models\Jurusan;
use App\Models\Kelas;
use Illuminate\Http\Request;
use Validator;

class JurusanController extends Controller
{
    public function index()
    {
        return view('admin.jurusan.index');
    }

    public function indexAjax()
    {
        $db = Jurusan::selectRaw("jurusan.*")->orderBy('jurusan.id', 'ASC');
        $data = $db->get();
        return datatables()->of($data)->addIndexColumn()->toJson();
    }

    public function getJurusan(Request $request)
    {
        try {
            $jurusan = Jurusan::selectRaw("jurusan.*")
                ->where('jurusan.id', $request->id)
                ->first();

            return response()->json([
                'status' => 'success',
                'data' => $jurusan,
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'status' => 'error',
                'icon' => 'error',
                'message' => $th->getMessage(),
            ]);
        }
    }

    public function store(Request $request)
    {
        $rules = [
            'nama_jurusan' => 'required|string|unique:jurusan,nama_jurusan'
        ];

        $validator  = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'validasi',
                'message' => $validator->errors()
            ]);
        }

        try {
            $jurusan = new Jurusan;
            $jurusan->nama_jurusan = $request->nama_jurusan;
            $jurusan->save();

            return response()->json([
                'status' => 'success',
                'icon' => 'success',
                'title' => 'Membuat Data Jurusan',
                'message' => 'Data Jurusan Baru Berhasil Dibuat',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'status' => 'error',
                'icon' => 'error',
                'title' => 'Membuat Data Jurusan',
                'message' => $th->getMessage(),
            ]);
        }
    }

    public function update(Request $request)
    {
        $rules = [
            'nama_jurusan' => 'required|string|unique:jurusan,nama_jurusan,' . $request->id_jurusan . '',
        ];

        $validator  = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'validasi',
                'message' => $validator->errors()
            ]);
        }

        try {
            $jurusan = Jurusan::findOrFail($request->id_jurusan);
            $jurusan->nama_jurusan = $request->nama_jurusan;
            $jurusan->save();

            return response()->json([
                'status' => 'success',
                'icon' => 'success',
                'title' => 'Update Jurusan',
                'message' => 'Jurusan Berhasil Diupdate',
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'error',
                'icon' => 'error',
                'title' => 'Update Jurusan',
                'message' => 'Jurusan Gagal diupdate',
            ]);
        }
    }

    public function destroy(Request $request)
    {
        try {
            Kelas::where('id_jurusan', $request->id)->delete();
            Jurusan::findOrFail($request->id)->delete();

            return response()->json([
                'status' => 'success',
                'icon' => 'success',
                'title' => 'Hapus Jurusan',
                'message' => 'Jurusan Berhasil Dihapus',
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'icon' => 'error',
                'title' => 'Hapus Jurusan',
                'message' => 'Jurusan Gagal Dihapus',
            ]);
        }
    }
}
