<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Field;
use App\Models\Participant;
use Illuminate\Http\Request;

class PesertaController extends Controller
{
    public function index(Event $event)
    {
        $data['event'] = $event;
        $data['bidang'] = Field::where('id_event', $event->id)->get();
        if (sizeof($data['bidang']) == 0) {
            $data['peserta'] = Participant::join('events as a', 'a.id', 'participants.id_event')
                ->join('users as b', 'b.id', 'participants.id_user')
                ->join('class as c', 'c.id', 'participants.id_kelas')
                ->selectRaw('participants.*, a.nama_event, b.name, c.nama_kelas')
                ->where('participants.id_event', $event->id)
                ->latest()
                ->get();
        } else {
            $data['peserta'] = Participant::join('events as a', 'a.id', 'participants.id_event')
                ->join('users as b', 'b.id', 'participants.id_user')
                ->join('class as c', 'c.id', 'participants.id_kelas')
                ->join('fields as d', 'd.id', 'participants.id_bidang')
                ->selectRaw('participants.*, a.nama_event, b.name, c.nama_kelas, d.nama_bidang')
                ->where('participants.id_event', $event->id)
                ->latest()
                ->get();
        }
        return view('admin.peserta.index', $data);
    }

    public function destroy($id)
    {
        Participant::findOrFail($id)->delete();
        return redirect()->back()->with('success', "Data Peserta Berhasil Dihapus !");
    }
}
