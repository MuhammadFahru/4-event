<?php

use App\Http\Controllers\EventController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InboxController;
use App\Http\Controllers\JurusanController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\KelasController;
use App\Http\Controllers\PesertaController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\User\AllEventController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('user.landing.index');
});

Route::get('/semua-event', [AllEventController::class, 'index'])->name('events');
Route::get('/semua-event/{event}/detail', [AllEventController::class, 'show'])->name('landing.event.show');
Route::get('/semua-event/event/search', [AllEventController::class, 'search'])->name('landing.event.search');


Route::get('/peserta', function () {
    return view('user.peserta.index');
});

Route::post('inbox/store', [InboxController::class, 'store'])->name('inbox.store');

Auth::routes();

Route::middleware('auth')->group(function () {

    Route::get('/dashboard', [HomeController::class, 'index'])->name('dashboard');

    // Event
    Route::get('/event', [EventController::class, 'index'])->name('event.index');
    Route::post('/event/ajax', [EventController::class, 'indexAjax'])->name('event.ajax');
    Route::get('/event/create', [EventController::class, 'create'])->name('event.create');
    Route::get('/event/search', [EventController::class, 'search'])->name('event.search');
    Route::patch('/verifikasi-data', [EventController::class, 'verifikasiData'])->name('verifikasi.data');
    Route::post('/data-event', [EventController::class, 'store'])->name('event.store');
    Route::get('/event/{event}/edit', [EventController::class, 'edit'])->name('event.edit');
    Route::delete('/bidang/{id}', [EventController::class, 'deleteBidang'])->name('event.delete-bidang');
    Route::patch('/data-event/{id}', [EventController::class, 'update'])->name('event.update');
    Route::delete('/data-event/{id}', [EventController::class, 'destroy'])->name('event.destroy');
    Route::get('/event/{event}/detail', [EventController::class, 'show'])->name('event.show');


    //Event Umum
    Route::get('/semua-event/{event}/pendaftaran', [AllEventController::class, 'form'])->name('landing.event.form');
    Route::post('/semua-event/{event}/pendaftaran/submit', [AllEventController::class, 'register'])->name('landing.event.register');


    Route::middleware('role:1')->group(function () {

        // User Routes
        Route::get('/users', [UserController::class, 'index'])->name('users.index');
        Route::get('/users/create', [UserController::class, 'create'])->name('users.create');
        Route::post('/users', [UserController::class, 'store'])->name('users.store');
        Route::get('/users/{id}/edit', [UserController::class, 'edit'])->name('users.edit');
        Route::patch('/users/{id}', [UserController::class, 'update'])->name('users.update');
        Route::delete('/users/{id}', [UserController::class, 'destroy'])->name('users.destroy');

        // Kategori Routes
        Route::get('/kategori', [KategoriController::class, 'index'])->name('kategori.index');
        Route::post('/kategori/ajax', [KategoriController::class, 'indexAjax'])->name('kategori.ajax');
        Route::post('/kategori', [KategoriController::class, 'store'])->name('kategori.store');
        Route::post('/kategori/update', [KategoriController::class, 'update'])->name('kategori.update');
        Route::post('/kategori/data', [KategoriController::class, 'getKategori'])->name('kategori.get');
        Route::post('/kategori/delete', [KategoriController::class, 'destroy'])->name('kategori.delete');

        // Jurusan Routes
        Route::get('/jurusan', [JurusanController::class, 'index'])->name('jurusan.index');
        Route::post('/jurusan/ajax', [JurusanController::class, 'indexAjax'])->name('jurusan.ajax');
        Route::post('/jurusan', [JurusanController::class, 'store'])->name('jurusan.store');
        Route::post('/jurusan/update', [JurusanController::class, 'update'])->name('jurusan.update');
        Route::post('/jurusan/data', [JurusanController::class, 'getjurusan'])->name('jurusan.get');
        Route::post('/jurusan/delete', [JurusanController::class, 'destroy'])->name('jurusan.delete');

        // Kelas Routes
        Route::get('/kelas', [KelasController::class, 'index'])->name('kelas.index');
        Route::post('/kelas/ajax', [KelasController::class, 'indexAjax'])->name('kelas.ajax');
        Route::post('/kelas', [KelasController::class, 'store'])->name('kelas.store');
        Route::post('/kelas/update', [KelasController::class, 'update'])->name('kelas.update');
        Route::post('/kelas/data', [KelasController::class, 'getKelas'])->name('kelas.get');
        Route::post('/kelas/delete', [KelasController::class, 'destroy'])->name('kelas.delete');


        // Pesan Routes
        Route::get('/inbox', [InboxController::class, 'index'])->name('inbox.index');
        Route::get('/inbox/{id}/detail', [InboxController::class, 'detail'])->name('inbox.detail');
        Route::get('/inbox/search', [InboxController::class, 'search'])->name('inbox.search');
        Route::delete('/inbox/{id}/delete', [InboxController::class, 'destroy'])->name('inbox.delete');
    });

    Route::middleware('role:1,2')->group(function () {

        // Peserta
        Route::get('/event/{event}/peserta', [PesertaController::class, 'index'])->name('peserta.index');
        Route::get('/event/{event}/peserta/ajax', [PesertaController::class, 'indexAjax'])->name('peserta.ajax');
        Route::delete('/peserta/{id}', [PesertaController::class, 'destroy'])->name('peserta.destroy');
    });
});
