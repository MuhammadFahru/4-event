<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_event')->unsigned();
            $table->bigInteger('id_user')->unsigned();
            $table->bigInteger('id_bidang')->unsigned();
            $table->bigInteger('id_kelas')->unsigned();
            $table->text('data_pendaftar');
            $table->string('dokumen_pendaftaran');
            $table->timestamps();

            $table->foreign('id_event')->references('id')->on('events');
            $table->foreign('id_user')->references('id')->on('users');
            $table->foreign('id_bidang')->references('id')->on('fields');
            $table->foreign('id_kelas')->references('id')->on('class');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participants');
    }
}
