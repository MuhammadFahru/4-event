<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_user')->unsigned();
            $table->bigInteger('id_kategori')->unsigned();
            $table->string('status_event');
            $table->string('jenis_pendaftar');
            $table->string('banner');
            $table->string('nama_event');
            $table->string('slug');
            $table->date('tanggal_mulai');
            $table->date('tanggal_selesai');
            $table->date('deadline_pendaftaran');
            $table->string('pelaksanaan');
            $table->string('lokasi')->nullable();
            $table->string('link');
            $table->text('deskripsi');
            $table->string('panduan_pendaftaran')->nullable();
            $table->boolean('dokumen_pendaftaran')->nullable();
            $table->timestamps();

            $table->foreign('id_user')->references('id')->on('users');
            $table->foreign('id_kategori')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
