<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert(array(
            [
                'name' => 'SMK Negeri 4 Bandung',
                'email' => 'admin@gmail.com',
                'no_telepon' => '081234567890',
                'username' => 'super.admin',
                'password' => Hash::make("super.admin"),
                'salt' => 'super.admin',
                'role_id' => 1,
                'is_active' => true,
                'status_identitas' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'SMK Negeri 4 Bandung',
                'email' => 'osis@gmail.com',
                'no_telepon' => '081234567890',
                'username' => 'akun.osis',
                'password' => Hash::make("akun.osis"),
                'salt' => 'akun.osis',
                'role_id' => 2,
                'is_active' => true,
                'status_identitas' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Event Organizer',
                'email' => 'eo@gmail.com',
                'no_telepon' => '081234567890',
                'username' => 'akun.eo',
                'password' => Hash::make("akun.eo"),
                'salt' => 'akun.eo',
                'role_id' => 3,
                'is_active' => true,
                'status_identitas' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Akun Guru',
                'email' => 'guru@gmail.com',
                'no_telepon' => '081234567890',
                'username' => 'akun.guru',
                'password' => Hash::make("akun.guru"),
                'salt' => 'akun.guru',
                'role_id' => 4,
                'is_active' => true,
                'status_identitas' => 0,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Akun Siswa',
                'email' => 'siswa@gmail.com',
                'no_telepon' => '081234567890',
                'username' => 'akun.siswa',
                'password' => Hash::make("akun.siswa"),
                'salt' => 'akun.siswa',
                'role_id' => 5,
                'is_active' => true,
                'status_identitas' => 0,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Akun Umum',
                'email' => 'umum@gmail.com',
                'no_telepon' => '081234567890',
                'username' => 'akun.umum',
                'password' => Hash::make("akun.umum"),
                'salt' => 'akun.umum',
                'role_id' => 6,
                'is_active' => true,
                'status_identitas' => 0,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ));
    }
}
