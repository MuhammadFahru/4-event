@extends('user.layouts.app')
@section('nav-peserta', 'active')

@section('content')

    {{-- Hero --}}
    <div class="hero-peserta">
        <h1 data-aos="fade-up" class="opening">Daftar Peserta <br>Rajin dan Malas</h1>
        <div class="d-flex justify-content-center mt-5">
            <a href="{{ route('dashboard') }}" class="btn btn-lg btn-info text-white mt-3 mr-2" data-aos="fade-up">Upload Event</a>
        </div>            
    </div>

    <div class="desc">
        <div class="container">
            <center data-aos="fade-up">
                <h1 class="title-section">Daftar Peserta Rajin</h1>
                <div class="line-title"></div>
            </center>
            <h4 class="mt-5 text-white" data-aos="fade-up">Belum Ada Data Yang Tersedia</h4>
            <br><br><br>
            <br><br><br>
            <center data-aos="fade-up">
                <h1 class="title-section">Daftar Peserta Malas</h1>
                <div class="line-title"></div>
            </center>
            <h4 class="mt-5 text-white" data-aos="fade-up">Belum Ada Data Yang Tersedia</h4>
        </div>
    </div>

@endsection
