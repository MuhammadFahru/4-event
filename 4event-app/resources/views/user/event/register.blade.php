@extends('user.layouts.app')
@section('nav-event', 'active')

@section('content')

<div class="container mb-3">
    <div><br><br><br><br></div>
    <div class="card shadow p-5">
        <div>
            <a href="#" onclick="history.go(-1)" class="btn btn-primary float-left mr-2"><i
                    class="fas fa-arrow-left"></i></a>
            <h2>Pendaftaran</h2>
        </div>
        <hr>

        <div>
            <form action="{{route('landing.event.register', $event->slug)}}" method="POST">
                @csrf
                <div class="mb-3 row">
                    <label class="col-sm-2 col-form-label">Nama Event</label>
                    <div class="col-sm-10">
                        <input type="text" disabled class="form-control-plaintext" id="staticEmail"
                            value="{{$event->nama_event}}">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label class="col-sm-2 col-form-label">Kategori</label>
                    <div class="col-sm-10">
                        <input type="text" disabled class="form-control-plaintext" value="{{$event->nama_kategori}}">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label class="col-sm-2 col-form-label">Tanggal Mulai</label>
                    <div class="col-sm-10">
                        <input type="text" disabled class="form-control-plaintext" value="{{ $event->tanggal_mulai->format('d F Y') }}">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label class="col-sm-2 col-form-label">Panduan</label>
                    <div class="col-sm-10">
                        <a href="#" class="btn btn-sm btn-primary"> <i class="fas fa-eye"></i> Lihat Panduan</a>
                        <a href="#" class="btn btn-sm btn-primary"><i class="fas fa-download"></i> Download Panduan</a>
                    </div>
                </div>

                <div class="mb-3 row">
                    <label class="col-sm-2 col-form-label">Bidang</label>
                    <div class="col-sm-10">
                        <select name="bidang" id="bidang" class="form-control" required>
                            <option value="">- Pilih Bidang -</option>
                            @foreach($event->bidang as $bidang)
                                <option value="{{$bidang->id}}">{{$bidang->nama_bidang}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>


                <div class="mt-5">
                    <h3>Datadiri</h3>
                    <hr>

                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Nama</label>
                        <div class="col-sm-10">
                            <input type="text" disabled class="form-control-plaintext" id="staticEmail" value="{{Auth::user()->name}}">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Kelas</label>
                        <div class="col-sm-10">
                            <select name="kelas" id="kelas" class="form-control" required>
                                <option value="">- Pilih Kelas -</option>
                                @foreach($kelas as $item)
                                    <option value="{{$item->id}}">{{$item->nama_kelas}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Data Pendaftar</label>
                        <div class="col-sm-10">
                            <textarea name="data_pendaftar" id="data_pendaftar" rows="5" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Dokumen Pendaftaran</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="file" id="dokumen_pendaftaran" name="dokumen_pendaftaran">
                        </div>
                    </div>
                </div>

                <div>
                    <button type="submit" class="btn btn-success float-right">Submit</button>
                </div>
            </form>
        </div>



    </div>
</div>
@endsection
