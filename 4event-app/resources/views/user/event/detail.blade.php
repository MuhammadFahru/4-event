@extends('user.layouts.app')
@section('nav-event', 'active')
@section('content')
<div class="container mb-3">
    <div><br><br><br><br></div>
    <div class="card shadow p-5">
        <div>
            <a href="#" onclick="history.go(-1)" class="btn btn-primary float-left mr-2"><i class="fas fa-arrow-left"></i></a>
            <h2>Detail Event</h2>
        </div>
        <hr>
        <div class="row">
            <div class="col-lg-12">
                <img src="{{ Storage::url($event->banner) }}" alt="Banner Event" class="w-100 rounded border mb-3">
                <div class="d-inline-block mb-2">
                    @if ($event->pelaksanaan == "Offline")
                        <h5 class="d-inline-block"><span class="badge badge-success p-2 mr-2 text-white">Offline</h5>
                    @elseif ($event->pelaksanaan == "Online")
                        <h5 class="d-inline-block"><span class="badge badge-warning p-2 mr-2 text-white">Online</h5>
                    @endif
                        <h5 class="d-inline-block"><span class="badge badge-info p-2 mr-2 text-white">{{ $event->nama_kategori }}</h5>
                    @if ($event->status_event == "Official")
                        <h5 class="d-inline-block"><span class="badge badge-primary p-2"><i class="fas fa-check-circle mr-1"></i>Official</h5>
                    @endif
                    
                </div>
                <div class="sharethis-inline-share-buttons d-inline float-right"></div>
                
                <h3 class="text-black">{{ $event->nama_event }}</h3>
                
                <hr class="my-4">
            </div>
            <div class="col-lg-4 mt-3">
                <h4 class="text-black"><i class="fas fa-calendar-alt mr-3 mb-2"></i>Jadwal Event</h4>
                <table class="w-100 text-black">
                    <tr>
                        <td width="130">Tanggal Mulai</td>
                        <td align="center">:</td>
                        <td>{{ $event->tanggal_mulai->format('d F Y') }}</td>
                    </tr>
                    <tr>
                        <td width="130">Tanggal Selesai</td>
                        <td align="center">:</td>
                        <td>{{ $event->tanggal_selesai->format('d F Y') }}</td>
                    </tr>
                </table>
            </div>
            <div class="col-lg-4 mt-3">
                <h4 class="text-black"><i class="far fa-clock mr-3 mb-2"></i>Deadline Pendaftaran</h4>
                <p class="text-black">{{ $event->deadline_pendaftaran->format('d F Y') }}</p>
            </div>
            <div class="col-lg-4 mt-3">
                <h4 class="text-black"><i class="fas fa-user-edit mr-3 mb-2"></i>Pendaftar</h4>
                <p class="text-black">{{ $event->jenis_pendaftar }}</p>
            </div>
            <div class="col-lg-4 mt-3">
                <h4 class="text-black"><i class="fas fa-map-marker-alt mr-3 mb-2"></i>Lokasi</h4>
                <p class="text-black">{{ $event->lokasi }}</p>
            </div>
            <div class="col-lg-4 mt-3">
                <h4 class="text-black"><i class="fas fa-list-ul mr-3 mb-2"></i>Kategori Event</h4>
                <p class="text-black">{{ $event->nama_kategori }}</p>
            </div>
            <div class="col-lg-4 mt-3">
                <h4 class="text-black"><i class="far fa-file-alt mr-3 mb-2"></i>Dokumen Pendaftaran</h4>
                <p class="text-black">{{ $event->dokumen_pendaftaran == 1 ? "Perlu kirim dokumen"  : "Tidak perlu kirim dokumen" }}</p>
            </div>
            <div class="col-lg-12 mt-3">
                <h4 class="text-black"><i class="fas fa-bars mr-3 mb-2"></i>Bidang</h4>
                <ul>
                    @foreach($event->bidang as $bidang)
                    <li class="text-black">{{$bidang->nama_bidang}}</li>
                    @endforeach
                </ul>
            </div>
            <div class="col-lg-12 mt-3">
                <h4 class="text-black"><i class="fas fa-book-open mr-3 mb-2"></i>Deskripsi Event</h4>
                <p class="text-black">{{ $event->deskripsi }}</p>
            </div>
            @if ($event->panduan_pendaftaran != null)
            <div class="col-lg-12 mt-3">
                <h4 class="text-black"><i class="fas fa-book mr-3 mb-2"></i>Panduan Pendaftaran Event</h4>
                <a href="{{ Storage::url($event->panduan_pendaftaran) }}" class="btn btn-primary w-100"><i
                        class="fas fa-eye mr-3"></i>Lihat File</a>
            </div>
            @endif

            <div class="col-lg-12 mt-3">
                <a href="{{ route('landing.event.form', $event->slug)}}" class="btn btn-primary float-right">Daftar Sekarang</a>
            </div>
        </div>
    </div>
</div>
@endsection

@push('sharethis')
<script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=607d8d7e8b3c240018665d08&product=inline-share-buttons" async="async"></script>
@endpush