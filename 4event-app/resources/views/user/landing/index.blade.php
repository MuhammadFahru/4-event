@extends('user.layouts.app')
@section('nav-home', 'active')

@section('content')
{{-- Hero --}}
<div class="hero-landing">
    <h1 data-aos="fade-up" class="opening">Selamat Datang di Aplikasi</h1>
    <img src="{{ asset('assets/img/logo_v2.png') }}" width="25%" data-aos="fade-up">
    <h4 data-aos="fade-up">Make Your Event Beneficial And Spectaculer</h4>
    <br>
    <div class="d-flex justify-content-center">
        <a href="{{ route('dashboard') }}" class="btn btn-lg btn-info text-white mt-3 mr-2" data-aos="fade-up">Upload
            Event</a>
        <!-- <a href="/peserta" class="btn btn-lg btn-info text-white mt-3 ml-2" data-aos="fade-up">Peserta</a> -->
    </div>
</div>

{{-- Penjelasan --}}
<div class="desc">
    <div class="container">
        <center data-aos="fade-up">
            <h1 class="title-section">Penjelasan</h1>
            <div class="line-title"></div>
        </center>
        <br><br><br>
        <div class="row mt-5">
            <div class="col-lg-6 text-left" data-aos="fade-left">
                <p class="deskripsi text-white text-justify">Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                    Excepturi enim qui ipsum ea aut iure dicta voluptatibus error, accusantium repudiandae accusamus
                    expedita corrupti exercitationem consectetur possimus atque id adipisci quisquam? Lorem ipsum dolor
                    sit amet consectetur, adipisicing elit. Fugiat voluptatem quis, accusantium placeat impedit odit
                    deserunt, voluptas voluptatum eaque, id quaerat facere minus! Molestias eum molestiae assumenda
                    nostrum dolorum repellendus. Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad labore
                    possimus dolorem velit quas aspernatur ea eligendi beatae tempora expedita quos aperiam mollitia
                    perspiciatis ipsa voluptas, cum quibusdam esse. Cum. Lorem ipsum dolor sit, amet consectetur
                    adipisicing elit. Nulla quidem corporis, labore explicabo dignissimos quod praesentium quis modi
                    debitis accusantium. Officiis qui ea provident nam porro.</p>
            </div>
            <div class="col-lg-6 text-right" data-aos="fade-right">
                <img src="{{ asset('assets/img/animation/what.png') }}" width="85%">
            </div>
        </div>
    </div>
</div>

{{-- Alur Proses --}}
<div class="proses">
    <center data-aos="fade-up">
        <h1 class="title-section">Alur Proses</h1>
        <div class="line-title"></div>
    </center>
    <br><br><br>
    <div class="row mt-5" data-aos="fade-up">
        <div class="col-lg-4 text-center">
            <img src="{{ asset('assets/img/animation/buat-event.png') }}" width="60%">
            <h2 class="mt-4"><b>Buat Event</b></h2>
            <p class="mb-0 text-justify mt-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eaque qui ipsam
                temporibus amet quisquam modi fugit ducimus neque rerum eveniet assumenda officia nam, nostrum minus?
                Est cum odio architecto alias!</p>
        </div>
        <div class="col-lg-4 text-center">
            <img src="{{ asset('assets/img/animation/upload-event.png') }}" width="68%" class="mt-4">
            <h2 class="mt-4"><b>Upload Event</b></h2>
            <p class="mb-0 text-justify mt-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eaque qui ipsam
                temporibus amet quisquam modi fugit ducimus neque rerum eveniet assumenda officia nam, nostrum minus?
                Est cum odio architecto alias!</p>
        </div>
        <div class="col-lg-4 text-center">
            <img src="{{ asset('assets/img/animation/peserta.png') }}" width="62%" class="mt-4">
            <h2 class="mt-4"><b>Dapatkan Peserta</b></h2>
            <p class="mb-0 text-justify mt-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eaque qui ipsam
                temporibus amet quisquam modi fugit ducimus neque rerum eveniet assumenda officia nam, nostrum minus?
                Est cum odio architecto alias!</p>
        </div>
    </div>
</div>

{{-- Gabung Sekarang --}}
<div class="gabung">
    <div class="container">
        <center data-aos="fade-up">
            <h1 class="title-section">Gabung Sekarang</h1>
            <div class="line-title"></div>
            <h4 class="mt-3">Coming Soon</h4>
            <div class="d-flex justify-content-center mt-5">
                <a href=""><img src="{{ asset('assets/img/button-google.png') }}" width="100%" class="mr-3"></a>
                <a href=""><img src="{{ asset('assets/img/button-apple.png') }}" width="100%" class="ml-3"></a>
            </div>
        </center>
    </div>
</div>

{{-- Hubungi Kami --}}
<div class="hubungi" id="kontak">
    <div class="container" >
        <center data-aos="fade-up">
            <h1 class="title-section">Hubungi Kami</h1>
            <div class="line-title"></div>
        </center>
        <br><br>
        <div class="row mt-5" >
            <div class="col-lg-8" data-aos="fade-right">
                <h2 class="text-left text-white"><b>Kirim Pesan</b></h2>
                <form action="{{ url('inbox/store') }}" method="POST" autocomplete="off" class="mt-4">
                    @csrf
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label class="text-white" for="nama">Nama</label>
                            <input type="text" class="form-control" id="nama" name="name" value="{{ old('name')  }}">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="text-white" for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="subject">Subjek</label>
                        <input type="text" class="form-control" id="subject" name="subject"
                            value="{{ old('subject') }}">
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="pesan">Pesan</label>
                        <textarea class="form-control" id="pesan" rows="9" style="resize: none;"
                            name="description">{{ old('description') }}</textarea>
                    </div>
                    <button type="submit" class="btn btn-primary w-25 float-right">Kirim</button>
                </form>
            </div>
            <div class="col-lg-4" data-aos="fade-left">
                <h2 class="text-left text-white"><b>Informasi Kontak</b></h2>
                <div class="card p-4 mt-4">
                    <div class="d-flex justify-content-start">
                        <i class="fas fa-phone-alt fa-4x mr-4" style="color: #6C63FF;"></i>
                        <div class="mt-1">
                            <h5>No Telepon :</h5>
                            <p class="mb-0">+62 898 6108 1920</p>
                        </div>
                    </div>
                </div>
                <div class="card p-4 mt-4">
                    <div class="d-flex justify-content-start">
                        <i class="fas fa-envelope fa-4x mr-4" style="color: #6C63FF;"></i>
                        <div class="mt-1">
                            <h5>Email :</h5>
                            <p class="mb-0">contact@4event.com</p>
                        </div>
                    </div>
                </div>
                <div class="card p-4 mt-4">
                    <div class="d-flex justify-content-start">
                        <i class="fas fa-map-marker-alt fa-4x mr-4 mt-3" style="color: #6C63FF;"></i>
                        <div class="mt-1">
                            <h5>Alamat :</h5>
                            <p class="mb-0">Jl. Kliningan No.6, Turangga, Kec. Lengkong, Kota Bandung, Jawa Barat 40264
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 mt-5" data-aos="fade-up">
                <h2 class="text-center text-white mt-5 mb-5"><b>Peta Lokasi</b></h2>
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.569558049065!2d107.62651371477303!3d-6.941934094984512!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e86413eb50ad%3A0xf9930b5268e3fee1!2sSMKN%204%20Bandung!5e0!3m2!1sid!2sid!4v1617070471024!5m2!1sid!2sid"
                    width="100%" height="550" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>
        </div>
    </div>
</div>



@if (Request::is('/#product'))
<script>
    $("document").ready(function () {
        $("#btnKontak").click(function() {
            $('html, body').animate({
                scrollTop: $("#kontak").offset().top
            }, 2000);
        });
    });

</script>
@endif

@endsection
