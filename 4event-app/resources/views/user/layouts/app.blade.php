<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>4 Event</title>

        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">

        <!-- Scripts -->
        
        <script src="{{ asset('assets/js/app.js') }}" defer></script>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

        <!-- Styles -->
        <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;300;400;500;600;700&display=swap" rel="stylesheet">        
        <script src="https://kit.fontawesome.com/f3e03d1e1d.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="{{ asset('assets/dash/vendor/sweetalert2/sweetalert2.min.css')}}">

        @stack('sharethis')
        

    </head>
    <body>
        @include('user.layouts.components.navbar')

        @yield('content')

        {{-- Footer --}}
        <div class="footer">
            <h5 class="mb-0">© Copyright 2019. All Rights Reserved.</h5>
        </div>
    
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

    <script type="text/javascript">
        AOS.init({
            duration : 1200,
        })
    </script>

    <script src="{{ asset('assets/dash/vendor/sweetalert2/sweetalert2.all.min.js')}}"></script>

    @if(Session::has('success'))
        <script>
            Swal.fire({
                icon: 'success',
                title: 'Terkirim',
                html: '{!! Session::get('success') !!}',
            })
        </script>
    @elseif(Session::has('error'))
        <script>
            Swal.fire({
                icon: 'error',
                title: 'Gagal',
                html: ' {!! Session::get('error') !!}',
            })
        </script>
    @elseif(Session::has('info'))
        <script>
            Swal.fire({
                icon: 'info',
                title: 'Info',
                html: ' {!! Session::get('info') !!}',
            })
        </script>
    @elseif($errors->any())
        <script>
            Swal.fire({
                icon: 'error',
                title: 'Gagal',
                html: '@foreach($errors->all() as $error) {!! $error."<br>" !!}@endforeach',
            })
        </script>
    @endif
    <script>
        $(function () {
            $(window).on('scroll', function () {
                if ( $(window).scrollTop() > 10 ) {
                    $('.navbar').addClass('solid');
                    $('.nav-link').addClass('active-scroll');
                } else {
                    $('.navbar').removeClass('solid');
                    $('.nav-link').removeClass('active-scroll');
                }
            });
        });
    </script>
    </body>

    
</html>
