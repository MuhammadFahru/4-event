<!-- Navbar-->
<header class="header">
    <nav class="navbar navbar-expand-lg fixed-top py-3">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{asset('/assets/img/logo_v2.png')}}" class="logo" alt="">
            </a>
            <button type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler navbar-toggler-right"><i class="fa fa-bars"></i></button>
            
            <div id="navbarSupportedContent" class="collapse navbar-collapse">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item ml-4">
                        <a class="nav-link @yield('nav-home')" href="{{url('/')}}">Home</a>
                    </li>
                    <li class="nav-item ml-4">
                        <a class="nav-link @yield('nav-event')" href="{{route('events')}}">Events</a>
                    </li>
                    <li class="nav-item ml-4">
                        <a class="nav-link @yield('nav-peserta')" href="{{url('/peserta')}}">Peserta</a>
                    </li>
                    <li class="nav-item ml-4">
                        <a class="nav-link @yield('berita')" href="#">Alur</a>
                    </li>
                    <li class="nav-item ml-4">
                        <a class="nav-link @yield('berita')" href="#">Tentang</a>
                    </li>
                    <li class="nav-item ml-4">
                        <a class="nav-link @yield('berita')" id="btnKontak" href="{{Request::is('/') ? '#kontak' : url('/').'#kontak'}}">Kontak</a>
                    </li>
                    <li class="nav-item ml-4">
                        @guest
                        <a href="{{route('login')}}"class="btn btn-primary rounded">Login</a>
                        @endguest
                        @auth
                        <a href="{{route('dashboard')}}"class="btn btn-primary rounded">Dashboard</a>
                        @endauth
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>