@extends('layouts.app')
@section('title', 'Login')

@section('content')
<div class="container">
    <br>
    <!-- Outer Row -->
    <div class="row justify-content-center mt-5">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-5 d-none d-lg-block bg-purple text-center">
                <a href="/">
                    <img src="{{ asset('assets/img/animation/login.svg') }}" width="90%" style="margin-top: 10em;">
                </a>
              </div>
              <div class="col-lg-7">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="text-gray-900 mb-4">Login</h1>
                  </div>
                  <hr>
                  <form method="POST" action="{{ route('login') }}">
                    @csrf
                    @if(session('error'))
                        <div class="alert alert-danger">
                            <i class="far fa-times-circle"></i>
                            {{ session('error') }} 
                        </div>
                    @endif
                    <div class="form-group">
                      <label for="username">Username</label>
                      <input type="text" class="form-control @error('username') is-invalid @enderror" id="username" name="username" value="{{ old('username') }}" placeholder="Masukan Username.....">
                      @error('username')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="password">Password</label>
                      <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password" value="{{ old('password') }}" placeholder="Masukan Password.....">
                      @error('password')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                    </div>
                    <div class="form-group form-check">
                      <input type="checkbox" class="form-check-input" id="exampleCheck1">
                      <label class="form-check-label" for="exampleCheck1">Ingat Saya</label>
                    </div>
                    <button type="submit" class="btn btn-primary w-100">Submit</button>
                  </form>
                  <hr>
                  <div class="text-center">
                    <div class="d-flex justify-content-center">
                        <h5 class="mb-0 mt-4 mr-2">Belum Punya Akun ?</h5>
                        <h5 class="mb-0 mt-4"><a href="/register">Register</a></h5>
                    </div>                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

</div>
@endsection
