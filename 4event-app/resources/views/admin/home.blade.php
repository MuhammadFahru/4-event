@extends('layouts.master')

@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-3">
    <div class="d-flex justify-content-start">
      <button class="btn btn-info mr-3"><i class="fas fa-fw fa-tachometer-alt fa-lg"></i></button>
      <h1 class="h3 text-gray-800 mt-2">Dashboard</h1>
    </div>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item">Home</li>
          <li class="breadcrumb-item active text-info">Dashboard</li>
        </ol>
    </nav>
</div>

@if (Auth::user()->role_id == 1)

<!-- Content Row -->
<div class="row">
  
    <div class="col-xl-12 col-md-12 mb-4">
      <div class="card shadow p-4">
        <div class="d-flex justify-content-start">
          <img src="" width="5%" id="iconGreeting">
          <h4 class="ml-3 mt-3" id="greeting"></h4>
        </div>
      </div>
    </div>
    
    <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-danger shadow h-100 py-2">
        <div class="card-body">
        <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Jumlah Event</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $event->count() }}</div>
              <a href="{{ route('event.index') }}" class="btn btn-danger mt-3">
                <p class="card-text">
                    <small>Lihat Detail <i class="fas fa-angle-double-right ml-2"></i></small>
                </p>
              </a>
            </div>
            <div class="col-auto">            
              <i class="fas fa-chart-bar fa-3x text-gray-300"></i>
            </div>
        </div>
        </div>
    </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body">
        <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Jumlah Kategori</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $kategori->count() }}</div>
              <a href="{{ route('kategori.index') }}" class="btn btn-success mt-3">
                <p class="card-text">
                    <small>Lihat Detail <i class="fas fa-angle-double-right ml-2"></i></small>
                </p>
              </a>
            </div>
            <div class="col-auto">            
              <i class="fas fa-chart-bar fa-3x text-gray-300"></i>
            </div>
        </div>
        </div>
    </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-info shadow h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Jumlah User</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $user->count() }}</div>
                    <a href="{{ route('users.index') }}" class="btn btn-info mt-3">
                      <p class="card-text">
                          <small>Lihat Detail <i class="fas fa-angle-double-right ml-2"></i></small>
                      </p>
                    </a>
                </div>
                <div class="col-auto">                    
                    <i class="fas fa-chart-bar fa-3x text-gray-300"></i>
                </div>
            </div>
        </div>
    </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-warning shadow h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Jumlah Pesan</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $pesan->count() }}</div>
                    <a href="{{ route('inbox.index') }}" class="btn btn-warning mt-3">
                      <p class="card-text">
                          <small>Lihat Detail <i class="fas fa-angle-double-right ml-2"></i></small>
                      </p>
                    </a>
                </div>
                <div class="col-auto">                    
                    <i class="fas fa-chart-bar fa-3x text-gray-300"></i>
                </div>
            </div>
        </div>
    </div>
    </div>    

</div>

<div class="row">

    <!-- Area Chart -->
    <div class="col-xl-8 col-lg-7">
      <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
          <h6 class="m-0 font-weight-bold text-primary">Statistik Participant Event</h6>          
        </div>
        <!-- Card Body -->
        <div class="card-body">
          <div class="chart-area">
            <canvas id="myAreaChart"></canvas>
          </div>
        </div>
      </div>
    </div>

    <!-- Pie Chart -->
    <div class="col-xl-4 col-lg-5">
      <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
          <h6 class="m-0 font-weight-bold text-primary">Statistik Jenis Event</h6>
        </div>
        <!-- Card Body -->
        <div class="card-body">
          <div class="chart-pie pt-4 pb-2">
            <canvas id="myPieChart"></canvas>
          </div>
          <div class="mt-4 text-center small">
            <span class="mr-2">
              <i class="fas fa-circle text-info"></i> Official
            </span>
            <span class="mr-2">
              <i class="fas fa-circle text-success"></i> Unofficial
            </span>
          </div>
        </div>
      </div>
    </div>
</div>

{{-- <div class="row">

    <!-- Area Chart -->
    <div class="col-xl-12 col-lg-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Projects</h6>
        </div>
        <div class="card-body">
          <h4 class="small font-weight-bold">Server Migration <span class="float-right">20%</span></h4>
          <div class="progress mb-4">
            <div class="progress-bar bg-danger" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
          <h4 class="small font-weight-bold">Sales Tracking <span class="float-right">40%</span></h4>
          <div class="progress mb-4">
            <div class="progress-bar bg-warning" role="progressbar" style="width: 40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
          <h4 class="small font-weight-bold">Customer Database <span class="float-right">60%</span></h4>
          <div class="progress mb-4">
            <div class="progress-bar" role="progressbar" style="width: 60%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
          <h4 class="small font-weight-bold">Payout Details <span class="float-right">80%</span></h4>
          <div class="progress mb-4">
            <div class="progress-bar bg-info" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
          <h4 class="small font-weight-bold">Account Setup <span class="float-right">Complete!</span></h4>
          <div class="progress">
            <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
        </div>
      </div>
    </div>

</div> --}}

@else

<!-- Content Row -->
<div class="row">    
    
  <div class="col-xl-12 col-md-12 mb-4">
  <div class="card border-left-info shadow h-100 py-2">
      <div class="card-body">
      @if (Auth::user()->status_identitas == 1)
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Jumlah Event</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $event->count() }}</div>
            <a href="{{ route('event.index') }}" class="mt-2 btn btn-info">Lihat Data <i class="fas fa-angle-double-right ml-2"></i></a>
          </div>
          <div class="col-auto">            
            <i class="fas fa-chart-bar fa-2x text-gray-300"></i>
          </div>
        </div>
      @else
        <h5 class="mb-0"><i class="fas fa-exclamation-triangle text-warning mr-2"></i> Anda Belum Melengkapi Bukti Identitas</h5>
      @endif
      </div>
  </div>
  </div>

  <div class="col-xl-12 col-md-12 mb-4">
    <div class="card shadow p-4">
      <div class="d-flex justify-content-start">
        <img src="" width="5%" id="iconGreeting">
        <h4 class="ml-3 mt-3" id="greeting"></h4>
      </div>
    </div>
  </div>

  <div class="col-xl-12 col-lg-12 col-md-12">
    <div class="card shadow mb-4 p-5">
      <div class="card-body text-center">
        <h1>Selamat Datang di Aplikasi</h1>
        <img src="{{ asset('assets/img/logo.png') }}" class="mt-3" alt="Logo 4 Event" width="50%">
      </div>
    </div>
  </div>

</div>

@endif

@endsection

@push('js')
    <script>
      var time = new Date().getHours();
      if (time < 11) {
        greeting = "Selamat Pagi";
        icon = "{{ asset('assets/img/icon/morning.png') }}";
      } else if (time < 17) {
        greeting = "Selamat Siang";
        icon = "{{ asset('assets/img/icon/sun.png') }}";
      } else {
        greeting = "Selamat Malam";
        icon = "{{ asset('assets/img/icon/moon.png') }}";
      }
      document.getElementById("iconGreeting").src = icon;
      document.getElementById("greeting").innerHTML = greeting + " {{ Auth::user()->name }}";
    </script>
@endpush
