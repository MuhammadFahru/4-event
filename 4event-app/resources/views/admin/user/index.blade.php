@extends('layouts.master')
@section('user', 'active')

@section('content')

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-3">
        <div class="d-flex justify-content-start">
            <button class="btn btn-info mr-3"><i class="fa fa-user fa-lg"></i></button>
            <h1 class="h3 text-gray-800 mt-2">Users</h1>
        </div>        
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Management</li>
            <li class="breadcrumb-item active text-info">Users</li>
            </ol>
        </nav>
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
        <div class="d-flex justify-content-between">
            <h4 class="m-0 mt-2 font-weight-bold text-primary">Data Users</h4>
            <a href="{{ route('users.create') }}" class="btn btn-primary"><i class="fas fa-plus mr-2"></i>Tambah Data</a>
        </div>
        </div>
        <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr align="center">
                    <th width="50">No</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Username</th>
                    <th>Role</th>
                    <th>Status</th>
                    <th width="150">Action</th>
                </tr>
            </thead>            
            <tbody>  
                @foreach ($user as $data)
                    <tr align="center">
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $data->name }}</td>
                        <td>{{ $data->email }}</td>
                        <td>{{ $data->username }}</td>
                        <td id="role_name">
                            @if ($data->role_id == 1)
                                <h5><span class="badge badge-secondary">Super Admin</span></h5>
                            @elseif ($data->role_id == 2)
                                <h5><span class="badge badge-secondary">OSIS</span></h5>
                            @elseif ($data->role_id == 3)
                                <h5><span class="badge badge-secondary">EO</span></h5>
                            @elseif ($data->role_id == 4)
                                <h5><span class="badge badge-secondary">Guru dan Staff</span></h5>
                            @elseif ($data->role_id == 5)
                                <h5><span class="badge badge-secondary">Siswa</span></h5>
                            @elseif ($data->role_id == 6)
                                <h5><span class="badge badge-secondary">Umum</span></h5>
                            @else
                                <h5><span class="badge badge-secondary">Tidak Terdaftar</span></h5>
                            @endif                            
                        </td>
                        <td id="status_active">
                            @if ($data->is_active == 1)
                                <h5><span class="badge badge-success">Active</span></h5>
                            @elseif ($data->is_active == 0)
                                <h5><span class="badge badge-warning">Non Active</span></h5>
                            @else
                                <h5><span class="badge badge-secondary">Tidak Terdaftar</span></h5>
                            @endif  
                        </td>
                        <td>
                            <a href="{{ url("users") }}/{{ $data->id }}/edit" class="btn btn-info w-25"><i class="fas fa-edit"></i></a>
                            <button class="btn btn-danger w-25" onclick="delete_onclick('{{ $data->id }}')"><i class="fas fa-trash-alt"></i></button>
                        </td>
                    </tr> 
                @endforeach                
            </tbody>
            </table>
        </div>
        </div>
    </div>

    <form method="post" id="delete_form">
        @method('DELETE')
        @csrf
    </form>

@endsection

@push('js')
    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable();
        });

        function delete_onclick(id) {
            Swal.fire({
                title: 'Apakah Anda yakin ?',
                icon: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#3085d6',
                confirmButtonColor: '#d33',
                cancelButtonText: 'Tidak',
                confirmButtonText: 'Ya'
            }).then((result) => {
                if (result.value) {
                    let form = document.getElementById('delete_form')
                    form.action = '{{ url("users") }}/'+id
                    form.submit()
                }
            })
        }
    </script>
@endpush
