@extends('layouts.master')
@section('user', 'active')

@section('content')

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-3">
        <div class="d-flex justify-content-start">
            <button class="btn btn-info mr-3"><i class="fa fa-user fa-lg"></i></button>
            <h1 class="h3 text-gray-800 mt-2">Users</h1>
        </div>        
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Management</li>
            <li class="breadcrumb-item active text-info">Form Users</li>
            </ol>
        </nav>
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="d-flex justify-content-start">
                <a href="{{ route('users.index') }}" class="btn btn-primary mr-3"><i class="fas fa-arrow-left"></i></a>
                <h4 class="m-0 mt-2 font-weight-bold text-primary">Form Users</h4>
            </div>
        </div>
        <div class="card-body">
            <form action="{{ url('users', @$user->id) }}" method="POST">
                @if (!empty($user))
                    @method('PATCH')
                @endif
                @csrf
                <div class="form-group">
                    <label for="name">Nama</label>
                    <input value="{{old('name', @$user->name)}}" type="text" class="form-control" id="name" placeholder="Masukan Nama" name="name" required>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input value="{{old('email', @$user->email)}}" type="text" class="form-control" id="email" placeholder="Masukan Email" name="email" required>
                </div>
                <div class="form-group">
                    <label for="no_telepon">No Telepon</label>
                    <input value="{{old('no_telepon', @$user->no_telepon)}}" type="text" class="form-control" id="no_telepon" placeholder="Masukan No Telepon" name="no_telepon" required>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="username">Username</label>
                        <input value="{{old('username', @$user->username)}}" type="text" class="form-control" id="username" placeholder="Masukan Username" name="username" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputPassword4">Password</label>
                        <input type="password" class="form-control" id="inputPassword4"
                                @if(empty($user))
                                    placeholder="Masukan Password" required
                                @else
                                    placeholder="Masukan Password bila ingin mengubahnya"
                                @endif
                                    name="password">
                    </div>
                </div>
                <div class="form-group">
                    <label for="role">Role</label>
                    <select id="role" name="role_id" class="form-control" required>
                        <option value="" disabled selected>-- Pilih --</option>
                        <option {{old('role_id', @$user->role_id) == 1 ? "selected" : ""}} value="1">Super Admin</option>
                        <option {{old('role_id', @$user->role_id) == 2 ? "selected" : ""}} value="2">OSIS</option>
                        <option {{old('role_id', @$user->role_id) == 3 ? "selected" : ""}} value="3">EO</option>
                        <option {{old('role_id', @$user->role_id) == 4 ? "selected" : ""}} value="4">Guru dan Staff</option>
                        <option {{old('role_id', @$user->role_id) == 5 ? "selected" : ""}} value="5">Siswa</option>
                        <option {{old('role_id', @$user->role_id) == 6 ? "selected" : ""}} value="6">Umum</option>
                    </select>
                </div>                      
                @if(!empty($user))
                    <div class="custom-control custom-checkbox mb-3">
                        <input type="checkbox" class="custom-control-input" name="is_active" value="1" id="active" @if(@$user->is_active == 1) checked @endif>
                        <label class="custom-control-label" for="active">Active</label>
                    </div>
                @endif
                <button type="submit" class="btn btn-primary"><i class="fa far fa-save mr-2"></i>Simpan</button>
            </form>
        </div>
    </div>

@endsection

@push('js')

@endpush
