@extends('layouts.master')
@section('event', 'active')

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="d-flex justify-content-start">
            <button class="btn btn-info mr-3"><i class="fas fa-chart-bar fa-lg"></i></button>
            <h1 class="h3 text-gray-800 mt-2">Event</h1>
        </div>        
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
            <li class="breadcrumb-item">Event</li>
            <li class="breadcrumb-item active text-info">Data Peserta</li>
            </ol>
        </nav>
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="d-flex justoify-content-start">
                <a href="{{ route('event.index') }}" class="btn btn-primary mr-3 pt-2-5"><i class="fas fa-arrow-left"></i></a>
                <h4 class="m-0 mt-2 font-weight-bold text-primary">Data Peserta Event {{ $event->nama_event }}</h4>
            </div>              
        </div>
        <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr align="center">
                    <th width="50">No</th>
                    <th>Nama Pendaftar</th>
                    @if (sizeof($bidang) > 0)
                        <th>Bidang</th>
                    @endif                    
                    <th>Kelas</th>
                    <th>Data Pendaftar</th>
                    @if ($event->dokumen_pendaftaran != 0)
                        <th>Dokumen Pendaftar</th>
                    @endif
                    <th>Action</th>
                </tr>
            </thead>            
            <tbody>
                @foreach ($peserta as $data)
                    <tr align="center">
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $data->name }}</td>
                        @if (!empty($data->id_bidang))
                            <td>{{ $data->nama_bidang }}</td>
                        @endif
                        <td>{{ $data->nama_kelas }}</td>
                        <td>{{ $data->data_pendaftar }}</td>
                        @if ($event->dokumen_pendaftaran != 0)
                            <td>{{ $data->dokumen_pemdaftaran }}</td>
                        @endif
                        <td><button type="button" class="btn btn-danger" onclick="delete_onclick('{{ $data->id }}')"><i class="fas fa-trash-alt"></i></button></td>                  
                    </tr>
                @endforeach     
            </tbody>
            </table>
        </div>
        </div>
    </div>

    <form method="post" id="delete_form">
        @method('DELETE')
        @csrf
    </form>

@endsection

@push('js')
    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable();
        });

        function delete_onclick(id) {
            Swal.fire({
                title: 'Apakah Anda yakin ?',
                icon: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#3085d6',
                confirmButtonColor: '#d33',
                cancelButtonText: 'Tidak',
                confirmButtonText: 'Ya'
            }).then((result) => {
                if (result.value) {
                    let form = document.getElementById('delete_form')
                    form.action = '{{ url("peserta") }}/'+id
                    form.submit()
                }
            })
        }
    </script>
@endpush