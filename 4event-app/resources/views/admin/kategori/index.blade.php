@extends('layouts.master')
@section('kategori', 'active')

@section('content')

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-3">
        <div class="d-flex justify-content-start">
            <button class="btn btn-info mr-3"><i class="fas fa-database fa-lg"></i></button>
            <h1 class="h3 text-gray-800 mt-2">Kategori</h1>
        </div>        
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
            <li class="breadcrumb-item">Master Data</li>
            <li class="breadcrumb-item active text-info">Kategori</li>
            </ol>
        </nav>
    </div>

    <input type="hidden" id="id_kategori" value="-1">

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
        <div class="d-flex justify-content-between">
            <h4 class="m-0 mt-2 font-weight-bold text-primary">Data Kategori</h4>
            <button type="button" onclick="addKategori()" class="btn btn-primary btn-sm"><i class="fas fa-plus mr-2"></i>Tambah Data</button>
        </div>
        </div>
        <div class="card-body">
        <button type="button" id="btn_search" class="btn btn-primary btn-sm d-none">Search Table</button>
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr align="center">
                        <th width="50">No</th>
                        <th>Nama Kategori</th>
                        <th>Tanggal Upload</th>
                        <th width="150">Action</th>
                    </tr>
                </thead>            
                <tbody>
                </tbody>
            </table>
        </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="titleModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="titleModal">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="bodyModal">
                    <form method="POST" id="formSimpan">                        
                        <div class="form-group">
                            <label for="nama_kategori">Nama Kategori</label>
                            <input type="text" class="reset_input form-control" id="nama_kategori" name="nama_kategori" autocomplete="off" required>
                            <div id="feedback_nama_kategori" class="reset_feedback"></div>
                        </div>                        
                        <button type="submit" id="btn_kategori" class="btn btn-primary float-right mt-2">Simpan</button>
                    </form>
                </div>        
            </div>
        </div>
    </div>

@endsection

@push('js')
    <script>
        $(document).ready(function() {

            $.ajaxSetup({ headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}, });
            
            var table_data = $('#dataTable').DataTable({            
                "searching" : true,
                "ordering" : true,
                "responsive": false,
                "fixedHeader": true,
                "processing": true,
                "serverSide": true,
                "language": {
                    "url" : "//cdn.datatables.net/plug-ins/1.10.21/i18n/Indonesian.json"
                },
                "ajax": {
                    "url": "{{route('kategori.ajax')}}",
                    "type": "POST",
                    "data": function (d) {
                        d._token = "{{ csrf_token() }}"
                    },
                },
                "columns": [
                    {"data": "DT_RowIndex", "name": "DT_RowIndex", "className": "text-center"},                    
                    {"data": "nama_kategori", "name": "nama_kategori", "className": "text-center"},                
                    {"data": "created_at", "name": "created_at", "className": "text-center",
                        render: function (data, type, row, meta) {
                            var date = moment(data, "YYYY-MM-DD").format('DD MMMM YYYY');
                            return date.toString();
                        },
                    },
                    {"data": null, "name": null, "className": "text-center", "orderable": false ,
                        render: function (data, type, row, meta) {
                            var btn = `<button onclick="editKategori('${data.id}')" class="btn btn-info"><i class="fas fa-edit"></i></button>
                                        <button onclick="deleteKategori('${data.id}')" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>`
                            return btn;
                        },
                    }
                ],
            });

            $("#btn_search").click(function(e){
                e.preventDefault();
                table_data.ajax.reload();
            });

            $("#formSimpan").submit(function(e){
                e.preventDefault();

                var id_kategori =  $("#id_kategori").val()
                var nama_kategori =  $("#nama_kategori").val()

                var formData = new FormData();
                formData.append('id_kategori', id_kategori);
                formData.append('nama_kategori', nama_kategori);
                formData.append('_token', $('meta[name="csrf-token"]').attr('content'));
                
                var link_route = "{{ route('kategori.store') }}"
                var text_swal = "Membuat Data Baru"
                var confirm_button_text = 'Ya, Simpan'
                if (id_kategori != '-1') {
                    link_route = "{{ route('kategori.update') }}"
                    var text_swal = "Mengupdate Data Menjadi "+ nama_kategori
                    var confirm_button_text = 'Ya, Update'
                }

                Swal.fire({
                    title: 'Apa kamu yakin ?',
                    text: text_swal,
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonText: confirm_button_text,
                    cancelButtonText: 'Cancel!',
                    reverseButtons: true,
                    showLoaderOnConfirm: true,
                    preConfirm: () => {
                        return fetch(link_route, {
                            method: "POST",
                            body: formData
                        }).then(response  => {
                            if (!response.ok) {
                                throw new Error(response.statusText)
                            }
                            return response.json()
                        }).catch(error => {
                            Swal.fire({ icon:'error', title:'Send Request Data', html:error,})
                        })
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (result.isConfirmed) {
                        var res = result.value
                        if (res.status == 'validasi') {
                            var arr_input = ['nama_kategori']
                            set_validator_input(arr_input, res)
                            return false;
                        }else if(res.status == 'success'){
                            resetForm();
                            $("#formModal").modal('hide')
                            $("#btn_search").click()
                        }                    
                        set_validator_reset()
                        Swal.fire({ icon: res.icon, title: res.title, html: res.message,})
                    }
                })
            });

        });

        function addKategori() {
            resetForm()
            $("#formModal").modal('show')
            $("#titleModal").text('Tambah Data Kategori')
            $("#btn_kategori").text('Simpan')
        }

        function resetForm(){
            $("#id_kategori").val('-1')
            $("#nama_kategori").val('')
        }

        function editKategori(id_kategori){
            resetForm()
            var formData = new FormData();
            formData.append('id', id_kategori);
            $.ajax({
                type: 'POST',
                url: "{{ route('kategori.get') }}",
                data: formData,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (response) {
                    if (response.status == 'success') {
                        $("#formModal").modal('show')
                        $("#titleModal").text('Update Kategori')
                        $("#btn_kategori").text('Update')
                        var res = response.data;
                        $("#id_kategori").val(res.id)
                        $("#nama_kategori").val(res.nama_kategori)
                    }else{
                        Swal.fire({ icon: response.icon, title: response.title, html: response.message,})
                    }
                },
            });
        }

        function deleteKategori(id_kategori){
            var formData = new FormData();
            formData.append('id', id_kategori);
            formData.append('_token', $('meta[name="csrf-token"]').attr('content'));

            Swal.fire({
                title: 'Apa kamu yakin ?',
                text: 'Menghapus data ini',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                confirmButtonText: 'Ya, Hapus!',
                cancelButtonText: 'Cancel!',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return fetch("{{ route('kategori.delete') }}", {
                        method: "POST",
                        body: formData
                    }).then(response  => {
                        if (!response.ok) {
                            throw new Error(response.statusText)
                        }
                        return response.json()
                    }).catch(error => {
                        Swal.fire({ icon:'error', title:'Send Request Data', html:error,})
                    })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.isConfirmed) {
                    var res = result.value
                    if (res.status == 'validasi') {
                        var arr_input = ['nama_kategori']
                        set_validator_input(arr_input, res)
                        return false;
                    }else if(res.status == 'success'){
                        resetForm();
                        $("#formModal").modal('hide')
                        $("#btn_search").click()
                    }
                    
                    set_validator_reset()
                    Swal.fire({ icon: res.icon, title: res.title, html: res.message,})
                }
            })
        }
    </script>
@endpush
