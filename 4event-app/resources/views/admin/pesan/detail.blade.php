@extends('layouts.master')
@section('pesan', 'active')

@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-3">
    <div class="d-flex justify-content-start">
        <button class="btn btn-info mr-3"><i class="fa fa-envelope fa-lg"></i></button>
        <h1 class="h3 text-gray-800 mt-2">Detail Pesan</h1>
    </div>        
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Pesan</li>
        <li class="breadcrumb-item active text-info">Detail Pesan</li>
        </ol>
    </nav>
</div>
<div class="card shadow mb-4">
    <div class="d-flex card-header justify-content-start py-3">
        <a href="{{ route('inbox.index') }}" class="btn btn-secondary mr-3"><i class="fas fa-arrow-left"></i></a>
        <h4 class="m-0 font-weight-bold text-primary mt-1">Isi Pesan</h4>
    </div>
    <div class="card-body">
        <h6 class="font-weight-bold">Tanggal Kirim</h6>
        <p class="small mb-3">{{ $inbox->created_at->format('d, M Y H:i') }}</p>
        <h6 class="font-weight-bold">Pengirim</h6>
        <p class="small mb-3">{{ $inbox->name }}</p>
        <h6 class="font-weight-bold">Email</h6>
        <p class="small mb-3">{{ $inbox->email }}</p>
        <h6 class="font-weight-bold">Subject</h6>
        <p class="small mb-3">{{ $inbox->subject }}</p>
        <h6 class="font-weight-bold">Content</h6>
        <p class="small mb-3">{{ $inbox->description }}</p>
        <div class="d-flex">
            <a href="" class="btn btn-danger mr-1" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-trash"></i> Hapus</a>
            <a href="https://mail.google.com/mail/u/0/?view=cm&fs=1&tf=1&to={{ $inbox->email }}" class="btn btn-info ml-1"><i class="fas fa-paper-plane"></i> Balas</a>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Anda yakin untuk menghapusnya ?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <div>
                        <div><small>Pengirim : {{ $inbox->name }}</small></div>
                        <div class="text-secondary">
                            <small>Tanggal Kirim : {{ $inbox->created_at->format('d F Y') }}</small>
                        </div>
                    </div>                    
                </div>
                <div class="modal-footer">
                    <form action="{{ url('inbox/' . $inbox->id . '/delete') }}" method="POST">
                        @csrf
                        @method('delete')
                        <button type="button" class="btn btn-secondary mr-2" data-dismiss="modal">Tidak</button>
                        <button class="btn btn-danger ml-2 mr-2" title="Delete" type="submit">Delete</button>
                    </form>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection