@extends('layouts.master')
@section('pesan', 'active')

@section('content')

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-3">
    <div class="d-flex justify-content-start">
        <button class="btn btn-info mr-3"><i class="fa fa-envelope fa-lg"></i></button>
        <h1 class="h3 text-gray-800 mt-2">Pesan Masuk</h1>
    </div>        
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Pesan</li>
        <li class="breadcrumb-item active text-info">Pesan Masuk</li>
        </ol>
    </nav>
</div>

<div class="card shadow mb-4">
    <div class="d-flex card-header justify-content-between py-3">
        <h4 class="m-0 mt-2 font-weight-bold text-primary">Semua Pesan Masuk</h4>
        <div class="d-flex">
            <a href="{{ route('inbox.index') }}" class="btn btn-link"><i class="fas fa-sync-alt"></i></a>
            <!-- Topbar Search -->
            <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search" action="{{  url('inbox/search') }}" method="GET">
                <div class="input-group">
                    <input type="text" name="search" class="form-control border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" value="{{ isset($search) ? $search : '' }}">
                    <div class="input-group-append">
                    <button class="btn btn-primary" type="submit">
                        <i class="fas fa-search fa-sm"></i>
                    </button>
                    </div>
                </div>
            </form>                            
        </div>
    </div>
    <div class="card-body">
        @forelse ($inbox as $item)                        
            <a href="{{ url('inbox/'.$item->id.'/detail') }}" class="noblue">
                <div class="row mail p-1 text-secondary mr-1 ml-1">
                    <div class="col-lg-1 small mt-1 text-center">{{ $loop->iteration }}</div>
                    <div class="col-lg-2 small mt-1">{{ $item->name }}</div>
                    <div class="col-lg-7 small mt-1"><b>{{ Str::limit($item->subject, 50) }}</b>, {{ Str::limit($item->description, 70) }}</div>
                    <div class="col-lg-2 small mt-1">{{ $item->created_at->diffForHumans() }}</div>
                </div>
            </a>
        @empty
            <center>No Data Available</center>
        @endforelse                                                
    </div>
</div>

@endsection