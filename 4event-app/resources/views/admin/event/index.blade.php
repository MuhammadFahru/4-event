@extends('layouts.master')
@section('event', 'active')

@section('content')
    
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="d-flex justify-content-start">
            <button class="btn btn-info mr-3"><i class="fas fa-chart-bar fa-lg"></i></button>
            <h1 class="h3 text-gray-800 mt-2">Event</h1>
        </div>        
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
            <li class="breadcrumb-item">Event</li>
            <li class="breadcrumb-item active text-info">Data Event</li>
            </ol>
        </nav>
    </div>    
    
    <hr class="my-4">
    <div class="d-flex justify-content-between">
        @if (Auth::user()->role_id == 1)
            <h1 class="h3 mb-0">Data Semua Event</h1>
        @else
            <h1 class="h3 mb-0">Event Dari {{ Auth::user()->name }}</h1>
        @endif            
        <a href="{{ route('event.create') }}" class="btn btn-primary"><i class="fas fa-upload mr-2"></i>Upload Event</a>
    </div>

    <div class="row">
        <div class="col-lg-12">
            
        </div>            
    </div>

    <div class="d-flex justify-content-start  mt-4 mb-2">
        <form class="d-sm-inline-block form-inline mr-auto navbar-search w-100" action="{{  url('event/search') }}" method="GET">
            <div class="input-group">
                <input type="text" name="search" class="form-control small w-50 mr-2" placeholder="Cari Event" aria-label="Search" aria-describedby="basic-addon2" value="{{ isset($search) ? $search : '' }}">                
                <select class="form-control mr-2" id="pelaksanaan_search" name="pelaksanaan_search">
                    <option disabled selected>Pilih Pelaksanaan</option>
                    <option value="Online" {{ @$pelaksanaan == "Online" ? 'selected' : '' }}>Online</option>
                    <option value="Offline" {{ @$pelaksanaan == "Offline" ? 'selected' : '' }}>Offline</option>
                </select>
                @if (Auth::user()->role_id == 1)
                    <select class="form-control mr-2" id="status_search" name="status_search">
                        <option disabled selected>Pilih Status</option>
                        <option value="Official" {{ @$status == "Official" ? 'selected' : '' }}>Official</option>
                        <option value="Unofficial" {{ @$status == "Unofficial" ? 'selected' : '' }}>Unofficial</option>
                    </select>
                @endif                
                <div class="input-group-append">
                <button class="btn btn-primary" type="submit">
                    <i class="fas fa-search fa-sm"></i>
                </button>
                </div>
            </div>
        </form>
        <a href="{{ route('event.index') }}" class="btn btn-primary ml-2"><i class="fas fa-sync-alt"></i></a>
    </div>

    <div class="row mb-5">
        @forelse ($event as $data)
            <div class="col-lg-4 mt-4">
                <div class="card shadow">
                    <a href="{{ url('event') }}/{{$data->slug}}/detail">
                        <img class="card-img-top event-banner" src="{{ Storage::url($data->banner) }}" alt="{{$data->nama_event}}">
                    </a>                        
                    <div class="card-body">
                        <div class="d-flex justify-content-start mb-1">
                            @if ($data->pelaksanaan == "Offline")
                                <h6><span class="badge badge-success p-2 mr-2">Offline</h6>
                            @elseif ($data->pelaksanaan == "Online")
                                <h6><span class="badge badge-warning p-2 mr-2">Online</h6>
                            @endif                                
                            <h6><span class="badge badge-info p-2 mr-2">{{ $data->nama_kategori }}</h6>                            
                            @if ($data->status_event == "Official")
                                <h6><span class="badge badge-primary p-2"><i class="fas fa-check-circle mr-1"></i>Official</h6>
                            @endif
                        </div>
                        <h5><a href="{{ url('event') }}/{{$data->slug}}/detail" class="noblue">{{$data->nama_event}}</a></h5>
                        <p><i class="far fa-calendar-alt mr-2"></i>{{ $data->tanggal_mulai->format('d F Y') }}</p>                            
                        <hr>
                        <div class="d-flex justify-content-between">
                            <div class="d-flex justify-content-start">
                                @if ($data->status_event == "Official")
                                    <img src="{{ asset('assets/dash/img/logo-smk.png') }}" alt="Pemilik Event" width="10%" class="ava-small mr-2">
                                @else
                                    <img src="{{ Storage::url($data->foto) }}" alt="Pemilik Event" width="10%" class="ava-small mr-2">
                                @endif                                
                                <p class="mt-2">{{ $data->name }}</p>
                            </div>
                            <div class="d-flex justify-content-start">
                                <a href="{{ url('event') }}/{{ $data->slug }}/edit" class="text-info mr-3 pt-2-5"><i class="far fa-edit"></i></a>
                                <a onclick="delete_onclick({{ $data->id }})" class="text-danger pt-2-5 c-pointer"><i class="far fa-trash-alt"></i></a>
                                @if ($data->status_event == "Official")
                                    <a href="{{ url('event') }}/{{ $data->slug }}/peserta" class="text-secondary ml-3 pt-2-5"><i class="fas fa-users"></i></a>
                                @endif
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
        @empty
            <div class="col-lg-12 text-center mt-5 p-5">
                <img src="{{ asset('assets/dash/img/empty.svg') }}" width="30%" class="mb-4">
                <h2>Belum Ada Data Yang Tersedia</h2>
            </div>
        @endforelse                      
    </div>

    <div class="d-flex justify-content-center mb-4">
        {{ $event->links() }}
    </div>    

    <form method="post" id="delete_form">
        @method('DELETE')
        @csrf
    </form>

@endsection

@push('js')
    <script>

    function delete_onclick(id) {
            Swal.fire({
                title: 'Apakah Anda Yakin ?',
                icon: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#3085d6',
                confirmButtonColor: '#d33',
                cancelButtonText: 'Tidak',
                confirmButtonText: 'Ya'
            }).then((result) => {
                if (result.value) {
                    let form = document.getElementById('delete_form')
                    form.action = '{{ url("data-event") }}/'+id
                    form.submit()
                }
            })
        }

        function buttonActive() {
            if (document.getElementById("konfirmasi-btn").checked) {
                var element = document.getElementById("next");
                element.classList.remove("disabled");
            } else {
                var element = document.getElementById("next");
                element.classList.add("disabled");
            }           
        }

        function changePage() {
            if (document.getElementById("konfirmasi-btn").checked) {
                document.getElementById("konfirmasi-tab").classList.remove("active");
                document.getElementById("konfirmasi").classList.remove("active");
                document.getElementById("konfirmasi").classList.remove("show");
                document.getElementById("input-tab").classList.add("active");
                document.getElementById("input").classList.add("active");
                document.getElementById("input").classList.add("show");
                document.getElementById("card-section").classList.remove("bg-dark");                
            } else {
                
            }
        }
    </script>
@endpush
