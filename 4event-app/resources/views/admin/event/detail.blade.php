@extends('layouts.master')
@section('event', 'active')

@section('content')
    
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="d-flex justify-content-start">
            <button class="btn btn-info mr-3"><i class="fas fa-chart-bar fa-lg"></i></button>
            <h1 class="h3 text-gray-800 mt-2">Event</h1>
        </div>        
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
            <li class="breadcrumb-item">Event</li>
            <li class="breadcrumb-item active text-info">Detail Event</li>
            </ol>
        </nav>
    </div>

    <div class="card shadow p-5">
        <div class="row">
            <div class="col-lg-12">
                <img src="{{ Storage::url($event->banner) }}" alt="Banner Event" class="w-100 rounded border mb-3">
                <div class="d-flex justify-content-start mb-2">
                    @if ($event->pelaksanaan == "Offline")
                        <h5><span class="badge badge-success p-2 mr-2">Offline</h6>
                    @elseif ($event->pelaksanaan == "Online")
                        <h5><span class="badge badge-warning p-2 mr-2">Online</h6>
                    @endif
                    <h5><span class="badge badge-info p-2 mr-2">{{ $event->nama_kategori }}</h6>
                    @if ($event->status_event == "Official")
                        <h6><span class="badge badge-primary p-2"><i class="fas fa-check-circle mr-1"></i>Official</h6>
                    @endif
                </div>                
                <h3 class="text-black">{{ $event->nama_event }}</h3>
                <hr class="my-4">
            </div>            
            <div class="col-lg-4 mt-2">
                <h4 class="text-black"><i class="fas fa-calendar-alt mr-3 mb-2"></i>Jadwal Event</h4>
                <table class="w-100 text-black">
                    <tr>
                        <td width="130">Tanggal Mulai</td>
                        <td align="center">:</td>
                        <td>{{ $event->tanggal_mulai->format('d F Y') }}</td>
                    </tr>
                    <tr>
                        <td width="130">Tanggal Selesai</td>
                        <td align="center">:</td>
                        <td>{{ $event->tanggal_selesai->format('d F Y') }}</td>
                    </tr>
                </table>
            </div>
            <div class="col-lg-4 mt-2">
                <h4 class="text-black"><i class="far fa-clock mr-3 mb-2"></i>Deadline Pendaftaran</h4>
                <p class="text-black">{{ $event->deadline_pendaftaran->format('d F Y') }}</p>
            </div>
            <div class="col-lg-4 mt-2">
                <h4 class="text-black"><i class="fas fa-user-edit mr-3 mb-2"></i>Pendaftar</h4>
                <p class="text-black">{{ $event->jenis_pendaftar }}</p>
            </div>
            <div class="col-lg-4 mt-4">
                <h4 class="text-black"><i class="fas fa-map-marker-alt mr-3 mb-2"></i>Lokasi</h4>
                <p class="text-black">{{ $event->lokasi }}</p>
            </div>            
            <div class="col-lg-4 mt-4">
                <h4 class="text-black"><i class="fas fa-list-ul mr-3 mb-2"></i>Kategori Event</h4>
                <p class="text-black">{{ $event->nama_kategori }}</p>
            </div>
            <div class="col-lg-4 mt-4">
                <h4 class="text-black"><i class="fas fa-link mr-3 mb-2"></i>Link Event</h4>
                @if ($event->link == "Pendaftaran Dilakukan Di Aplikasi")
                    <a href="" class="noblue text-info">Daftar Sekarang</a>
                @else
                    <a href="{{ $event->link }}" class="noblue text-info">Link Event</a>
                @endif                
            </div>
            <div class="col-lg-12 mt-3">
                <h4 class="text-black"><i class="fas fa-book-open mr-3 mb-2"></i>Deskripsi Event</h4>
                <p class="text-black">{{ $event->deskripsi }}</p>
            </div>
            @if ($event->panduan_pendaftaran != null)
                <div class="col-lg-12 mt-3">
                    <h4 class="text-black"><i class="fas fa-book mr-3 mb-2"></i>Panduan Pendaftaran Event</h4>
                    <a href="{{ Storage::url($event->panduan_pendaftaran) }}" class="btn btn-primary w-100"><i class="fas fa-eye mr-3"></i>Lihat File</a>
                </div>
            @endif            
        </div>
    </div>

@endsection