@extends('layouts.master')
@section('event', 'active')

@section('content')    

    @if (Auth::user()->status_identitas == 1)

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="d-flex justify-content-start">
            <button class="btn btn-info mr-3"><i class="fas fa-chart-bar fa-lg"></i></button>
            <h1 class="h3 text-gray-800 mt-2">Event</h1>
        </div>        
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
            <li class="breadcrumb-item">Event</li>
            <li class="breadcrumb-item active text-info">Form Event</li>
            </ol>
        </nav>
    </div>

    <div class="card shadow p-4 mb-4 rounded">
        <div class="d-flex justoify-content-start">
            <a href="{{ route('event.index') }}" class="btn btn-primary mr-3 pt-2-5"><i class="fas fa-arrow-left"></i></a>
            <h3 class="mt-2">Form Input Event</h3>
        </div>        
        <hr class="mt-3 mb-4">
        <form action="{{ url('data-event', @$event->id) }}" method="POST" enctype="multipart/form-data">
            @if (!empty($event))
                @method('PATCH')
            @else             
                @method('POST')    
            @endif
            @csrf
            <div class="form-group">
                <label for="customFile2" class="mb-2 text-black">Banner Event <sup class="text-danger">*</sup></label><br>
                <img src="<?= !empty($event->banner) ? Storage::url($event->banner) : asset('assets/dash/img/img_not_found.jpg') ?>" class="mx-auto mb-3 banner-event border" id="output3">
                <div class="custom-file mt-1">
                    <input type="file" class="custom-file-input" id="customFile2" onchange="loadFile3(event)" accept=".jpg,.png,.jpeg" name="banner" {{ @$event->banner == null ? "required" : "" }}>
                    <label class="custom-file-label" for="customFile2">Pilih file</label>
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <div class="form-group">
                        <label class="text-black" for="nama_event">Nama Event <sup class="text-danger">*</sup></label>
                        <input type="text" class="form-control" id="nama_event" name="nama_event" placeholder="Masukan Nama Event" required value="{{ old('nama_event', @$event->nama_event) }}">
                    </div>                  
                </div>
                <div class="col">
                    <div class="form-group">
                        <label class="text-black" for="kategori_event">Kategori Event <sup class="text-danger">*</sup></label>
                        <select class="form-control" id="kategori_event" name="kategori_event" required>
                            <option selected disabled>--- Pilih Kategori Event ---</option>
                            @foreach ($kategori as $data)
                                <option {{ $data->id == @$event->id_kategori ? "selected" : "" }} value="{{ $data->id }}">{{ $data->nama_kategori }}</option>
                            @endforeach                            
                        </select>
                    </div>                    
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <div class="form-group">
                        <label class="text-black" for="tanggal_mulai">Tanggal Mulai <sup class="text-danger">*</sup></label>
                        <div class="input-group">
                            <input type="text" class="reset_input form-control datepicker1" id="tanggal_mulai" name="tanggal_mulai" autocomplete="off" value="{{ old('tanggal_mulai', @$event->tanggal_mulai == null ? "" :  @$event->tanggal_mulai->format('Y-m-d')) }}" placeholder="Masukan Tanggal Mulai Event" required>
                            <div id="feedback_tanggal_mulai" class="reset_feedback"></div>
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                            </div>
                        </div>
                    </div>    
                </div>
                <div class="col">
                    <div class="form-group">
                        <label class="text-black" for="tanggal_selesai">Tanggal Selesai <sup class="text-danger">*</sup></label>
                        <div class="input-group">
                            <input type="text" class="reset_input form-control datepicker1" id="tanggal_selesai" name="tanggal_selesai" autocomplete="off" value="{{ old('tanggal_selesai',@$event->tanggal_selesai == null ? "" :  @$event->tanggal_selesai->format('Y-m-d')) }}" placeholder="Masukan Tanggal Selesai Event" required>
                            <div id="feedback_tanggal_selesai" class="reset_feedback"></div>
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <div class="form-group">
                        <label class="text-black" for="jenis_pendaftar">Jenis Pendaftar <sup class="text-danger">*</sup></label>
                        <select class="form-control" id="jenis_pendaftar" name="jenis_pendaftar" required>
                            <option selected disabled>--- Pilih Jenis Pendaftar ---</option>
                            <option {{old('jenis_pendaftar', @$event->jenis_pendaftar) == "Solo" ? "selected" : ""}} value="Solo">Solo</option>
                            <option {{old('jenis_pendaftar', @$event->jenis_pendaftar) == "Duo" ? "selected" : ""}} value="Duo">Duo</option>
                            <option {{old('jenis_pendaftar', @$event->jenis_pendaftar) == "Grup" ? "selected" : ""}} value="Grup">Grup</option>
                            <option {{old('jenis_pendaftar', @$event->jenis_pendaftar) == "Campuran" ? "selected" : ""}} value="Campuran">Campuran</option>
                        </select>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label class="text-black" for="deadline_pendaftaran">Deadline Pendaftaran <sup class="text-danger">*</sup></label>
                        <div class="input-group">
                            <input type="text" class="reset_input form-control datepicker1" id="deadline_pendaftaran" name="deadline_pendaftaran" autocomplete="off" value="{{ old('deadline_pendaftaran', @$event->deadline_pendaftaran == null ? "" :  @$event->deadline_pendaftaran->format('Y-m-d')) }}" placeholder="Masukan Tanggal Selesai Event" required>
                            <div id="feedback_deadline_pendaftaran" class="reset_feedback"></div>
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row">
                @if (Auth::user()->role_id == 3 || Auth::user()->role_id == 4 || Auth::user()->role_id == 5 || Auth::user()->role_id == 6)
                    <div class="col">
                        <div class="form-group">
                            <label class="text-black" for="link">Link Pendaftaran Event <sup class="text-danger">*</sup></label>
                            <input type="text" class="form-control" id="link" name="link" placeholder="Masukan Link Pendaftaran Event" required value="{{ old('link', @$event->link) }}">
                        </div> 
                    </div>
                @else
                    <div class="col d-none">
                        <div class="form-group">
                            <label class="text-black" for="link">Link Pendaftaran Event <sup class="text-danger">*</sup></label>
                            <input type="text" class="form-control" id="link" name="link" placeholder="Masukan Link Pendaftaran Event" required value="Pendaftaran Dilakukan Di Aplikasi">
                        </div> 
                    </div>
                @endif                
                <div class="col">
                    <div class="form-group">
                        <label class="text-black" for="pelaksanaan">Pelaksanaan Event <sup class="text-danger">*</sup></label>
                        <select class="form-control" id="pelaksanaan" name="pelaksanaan" required onchange="getComboA(this)">
                            <option selected disabled>--- Pilih Pelaksanaan Event ---</option>
                            <option {{old('jenis_pendaftar', @$event->pelaksanaan) == "Online" ? "selected" : ""}} value="Online">Online</option>
                            <option {{old('jenis_pendaftar', @$event->pelaksanaan) == "Offline" ? "selected" : ""}} value="Offline">Offline</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group" id="lokasi-section">
                <label class="text-black" for="lokasi">Lokasi Event</label>
                <textarea class="form-control r-none" id="lokasi" rows="5" name="lokasi" placeholder="Masukan Lokasi Event">{{ old('lokasi', @$event->lokasi) }}</textarea>
            </div>
            <div class="form-group">
                <label class="text-black" for="deskripsi">Deskripsi Event <sup class="text-danger">*</sup></label>
                <textarea class="form-control r-none" id="deskripsi" rows="10" name="deskripsi" placeholder="Masukan Deskripsi Event" required>{{ old('deskripsi', @$event->deskripsi) }}</textarea>
            </div>            
            @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
                <div class="form-group">
                    <label class="text-black" for="customFile" class="mb-2">Panduan Pendaftaran Event</label>
                    <div class="d-flex justify-content-start">
                        <div class="custom-file mt-1 mr-2 w-100">
                            <input type="file" class="custom-file-input" id="customFile" accept=".jpg,.png,.jpeg,.pdf" name="panduan_pendaftaran">
                            <label class="custom-file-label" for="customFile">Pilih file</label>
                        </div>
                        @if (!empty($event->panduan_pendaftaran))
                            <a href="{{ Storage::url($event->panduan_pendaftaran) }}" class="btn btn-primary pt-2 w-25">Lihat File <i class="fas fa-eye ml-2"></i></a>
                        @endif                        
                    </div>                    
                </div>
                <div class="form-group">
                    <div class="custom-control custom-checkbox mt-3">
                        <input type="checkbox" class="custom-control-input" name="dokumen_pendaftaran" value="1" id="dokumen_pendaftaran" @if(@$event->dokumen_pendaftaran == 1) checked @endif>
                        <label class="custom-control-label" for="dokumen_pendaftaran">Perlu Dokumen Pendaftar</label>
                    </div>
                </div>
                <div class="form-group">         
                    <label for="bidang" class="text-black">Bidang Event</label>                      
                    <div class="input-group-btn"> 
                        <button class="btn btn-success btn-success-add" type="button"><i class="fas fa-plus"></i> Tambah Bidang</button>
                    </div>
                    <div class="input-group increment"></div>
                    @if (!empty($bidang))
                        @if (sizeof($bidang) > 0)
                            @foreach ($bidang as $data)
                                <div class="clone">
                                    <div class="control-group input-group mt-3">
                                        <div class="input-group-btn"> 
                                            <button class="btn btn-danger rounded-0" type="button" onclick="delete_onclick({{ $data->id }})"><i class="fas fa-times"></i> Hapus</button>
                                        </div>
                                        <input type="text" class="form-control" id="bidang" name="bidang[]" value="{{ $data->nama_bidang }}">
                                        <input type="hidden" class="form-control d-none" id="id_bidang" name="id_bidang[]" value="{{ $data->id }}">
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="clone">
                                <div class="control-group input-group mt-3">
                                    <div class="input-group-btn"> 
                                        <button class="btn btn-danger btn-danger-del rounded-0" type="button"><i class="fas fa-times"></i> Hapus</button>
                                    </div>
                                    <input type="text" class="form-control" id="bidang" name="bidang[]" value="{{ old('bidang') }}">
                                </div>
                            </div>
                        @endif
                    @else
                        <div class="clone">
                            <div class="control-group input-group mt-3">
                                <div class="input-group-btn"> 
                                    <button class="btn btn-danger btn-danger-del rounded-0" type="button"><i class="fas fa-times"></i> Hapus</button>
                                </div>
                                <input type="text" class="form-control" id="bidang" name="bidang[]" value="{{ old('bidang') }}">
                            </div>
                        </div>
                    @endif
                </div>                
            @endif            
            <br>
            <div class="form-group">
                <button type="submit" class="btn btn-success float-right w-25">Submit Data</button>
            </div>
        </form>
    </div>

    @else
    
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="d-flex justify-content-start">
            <button class="btn btn-info mr-3"><i class="fas fa-chart-bar fa-lg"></i></button>
            <h1 class="h3 text-gray-800 mt-2">Event</h1>
        </div>        
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
            <li class="breadcrumb-item">Event</li>
            <li class="breadcrumb-item active text-info">Verifikasi Data Pengguna</li>
            </ol>
        </nav>
    </div>

    <div class="card shadow text-center p-5 mb-4 bg-dark rounded" id="card-section">
        <ul class="nav nav-tabs d-none" id="myTab" role="tablist">
            <li class="nav-item" role="presentation" id="tab-konfirmasi">
                <a class="nav-link active" id="konfirmasi-tab" data-toggle="tab" href="#konfirmasi" role="tab" aria-controls="konfirmasi" aria-selected="true">Konfirmasi</a>
            </li>
            <li class="nav-item d-none" role="presentation" id="tab-input">
                <a class="nav-link" id="input-tab" data-toggle="tab" href="#input" role="tab" aria-controls="input" aria-selected="false">Input Data</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade active show pt-3" id="konfirmasi" role="tabpanel" aria-labelledby="konfirmasi-tab">
                <div class="row">                        
                    <div class="col-lg-6 text-white">
                        <h3>Konfirmasi Upload Event</h3>
                        <p class="text-justify w-75 mx-auto mt-3">Anda diwajibkan melengkapi bukti identitas sebelum mengupload event. Demi keamanan dan kenyamanan bersama, kami memerlukan verifikasi pengguna untuk upload event.</p>
                        <p class="text-justify w-75 mx-auto">Tindakan yang melanggar hukum, seperti <i>penipuan</i> akan dilaporkan ke pihak berwenang.</p>
                        <p class="text-justify w-75 mx-auto">Data yang anda input akan dirahasiakan dan hanya akan digunakan untuk verifikasi.</p>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="konfirmasi-btn" onclick="buttonActive()">
                            <label class="custom-control-label" for="konfirmasi-btn">Saya Ingin Upload Event dan Menyetujui Verifikasi Data</label>
                        </div>
                        <button type="button" class="btn btn-success w-75 mt-3 disabled" id="next" onclick="changePage()">Selanjutnya</button>
                    </div>
                    <div class="col-lg-6">
                        <br>
                        <img src="{{ asset('assets/dash/img/verifikasi.svg') }}" alt="Block Animated" width="80%" class="mx-auto">
                    </div>
                </div>
            </div>
            <div class="tab-pane fade pt-3 text-left" id="input" role="tabpanel" aria-labelledby="input-tab">
                <form action="{{ url('verifikasi-data') }}" method="POST" enctype="multipart/form-data">
                    @method('PATCH')
                    @csrf
                    <div class="row">
                        <div class="col-lg-12">
                            <h3>Form Verifikasi Data Pengguna</h3>
                            <hr class="my-4">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="customFile" class="mb-4">Foto Profil</label><br>
                                        <br>
                                        <center>
                                            <img src="{{ asset('assets/dash/img/user.png') }}" class="mx-auto border-radius-circle shadow img-circle" id="output">
                                        </center>                                        
                                        <br>
                                        <div class="custom-file mt-4">
                                            <input type="file" class="custom-file-input" id="customFile" onchange="loadFile(event)" accept=".jpg,.png,.jpeg" name="foto" required>
                                            <label class="custom-file-label text-left" for="customFile">Pilih File</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="customFile" class="mb-3">Bukti Identitas</label><br>
                                        <img src="{{ asset('assets/dash/img/card-layout.jpg') }}" class="mx-auto shadow mb-3 card-identitas" id="output2">
                                        <div class="custom-file mt-4">
                                            <input type="file" class="custom-file-input" id="customFile" onchange="loadFile2(event)" accept=".jpg,.png,.jpeg" name="bukti_identitas" required>
                                            <label class="custom-file-label" for="customFile">Pilih file</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="jenis_kelamin">Jenis Kelamin</label><br>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="laki_laki" name="jenis_kelamin" class="custom-control-input" value="Laki - Laki" {{ old('jenis_kelamin') == "Laki - Laki" ? 'checked' : '' }} required>
                                    <label class="custom-control-label" for="laki_laki">Laki - Laki</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="perempuan" name="jenis_kelamin" class="custom-control-input" value="Perempuan" {{ old('jenis_kelamin') == "Perempuan" ? 'checked' : '' }} required>
                                    <label class="custom-control-label" for="perempuan">Perempuan</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="lembaga" name="jenis_kelamin" class="custom-control-input" value="Lembaga" {{ old('jenis_kelamin') == "Lembaga" ? 'checked' : '' }} required>
                                    <label class="custom-control-label" for="lembaga">Lembaga</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tanggal_lahir">Tanggal Lahir</label>
                                <div class="input-group">
                                    <input type="text" class="reset_input form-control datepicker1" id="tanggal_lahir" name="tanggal_lahir" autocomplete="off" value="{{ old('tanggal_lahir') }}" required>
                                    <div id="feedback_tanggal_lahir" class="reset_feedback"></div>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tempat_lahir">Tempat Lahir</label>
                                <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" value="{{ old('tempat_lahir') }}" required>
                            </div>
                            <div class="form-group">
                                <label for="alamat">Alamat</label>
                                <textarea class="form-control r-none" id="alamat" rows="5" name="tempat_tinggal" required>{{ old('tempat_tinggal') }}</textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success float-right w-25">Submit Data</button>
                            </div>     
                        </div>
                    </div>
                </form>                    
            </div>
        </div>                                    
    </div>
    @endif
    
    <form method="post" id="delete_form">
        @method('DELETE')
        @csrf
    </form> 
        
@endsection

@push('js')
    <script type="text/javascript">
        $(document).ready(function() { 
            $("body").on("click",".btn-success-add",function(){                
                var html = $(".clone").html();
                $(".increment").after(html);
                $("#bidang").val("");
                $("#id_bidang").val("");
            });
            $("body").on("click",".btn-danger-del",function(){ 
                $(this).parents(".control-group").remove();
            });
        });

        function delete_onclick(id) {
            Swal.fire({
                title: 'Apakah Anda Yakin ?',
                icon: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#3085d6',
                confirmButtonColor: '#d33',
                cancelButtonText: 'Tidak',
                confirmButtonText: 'Ya'
            }).then((result) => {
                if (result.value) {
                    let form = document.getElementById('delete_form')
                    form.action = '{{ url("bidang") }}/'+id
                    form.submit()
                }
            })
        }
    </script>
@endpush
