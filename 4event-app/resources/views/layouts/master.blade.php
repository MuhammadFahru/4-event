<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>4 Event | Dashboard</title>

  <!-- Custom fonts for this template -->
  <link href="{{ asset('assets/dash/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="{{ asset('assets/dash/css/dashboard.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('assets/dash/vendor/sweetalert2/sweetalert2.min.css')}}">
  <link rel="stylesheet" href="{{ asset('assets/dash/vendor/select2/css/select2.min.css')}}">
  <link rel="stylesheet" href="{{ asset('assets/dash/vendor/select2/css/select2-bootstrap-theme.min.css')}}">

  <!-- Custom styles for this page -->
  <link href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css" rel="stylesheet">  

  @stack('css')

</head>

<body id="page-top">

  {{-- @include('sweetalert::alert') --}}

  <!-- Page Wrapper -->
  <div id="wrapper">

    @include('layouts.components.sidebar')

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        @include('layouts.components.topbar')

        <!-- Begin Page Content -->
        <div class="container-fluid">          
            @yield('content')
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; 4 Event {{ date('Y') }}. All Rights Reserved</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Yakin Untuk Logout ?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <form id="logout-form" action="{{ route('logout') }}" method="POST">
            @csrf
            <button type="submit" class="btn btn-danger">Logout</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="{{ asset('assets/dash/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/dash/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{ asset('assets/dash/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{ asset('assets/dash/js/dashboard.js') }}"></script>
  <script src="{{ asset('assets/dash/js/moment.js') }}"></script>

  <!-- Page level plugins -->
  <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap4.min.js"></script>

  <!-- Page level plugins -->
  <script src="{{ asset('assets/dash/vendor/chart.js/Chart.min.js') }}"></script>

  <!-- Page level custom scripts -->
  <script src="{{ asset('assets/dash/js/demo/chart-area-demo.js') }}"></script>
  <script src="{{ asset('assets/dash/js/demo/chart-pie-demo.js') }}"></script>
  <script src="{{ asset('assets/dash/js/demo/chart-bar-demo.js') }}"></script>

  <script src="{{ asset('assets/dash/vendor/sweetalert2/sweetalert2.all.min.js')}}"></script>

  <script src="{{ asset('assets/dash/vendor/select2/js/select2.min.js')}}"></script>

  <script src="{{ asset('assets/js/js_global.js') }}"></script>

  <!-- DatePicker -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet"/>

  <script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>

  <script>
      $(".datepicker1").datepicker({
        format: 'yyyy-mm-dd'
      });

      $(".datepicker2").datepicker({
        format: 'yyyy-mm-dd'
      });

      $(".datepicker3").datepicker({
        format: 'yyyy-mm-dd'
      });

      $(".datepicker4").datepicker({
        format: 'yyyy-mm-dd'
      });

      $(document).ready(function () {
        bsCustomFileInput.init()
      })

      var loadFile = function(event) {
        var reader = new FileReader();
        reader.onload = function(){
          var output = document.getElementById('output');
          output.src = reader.result;
        };
        reader.readAsDataURL(event.target.files[0]);
      };

      var loadFile2 = function(event) {
        var reader2 = new FileReader();
        reader2.onload = function(){
          var output2 = document.getElementById('output2');
          output2.src = reader2.result;
        };
        reader2.readAsDataURL(event.target.files[0]);
      };

      var loadFile3 = function(event) {
        var reader3 = new FileReader();
        reader3.onload = function(){
          var output3 = document.getElementById('output3');
          output3.src = reader3.result;
        };
        reader3.readAsDataURL(event.target.files[0]);
      };
  </script>

  @if(Session::has('success'))
      <script>
          Swal.fire({
              icon: 'success',
              title: 'Sukses',
              html: '{!! Session::get('success') !!}',
          })
      </script>
  @elseif(Session::has('error'))
      <script>
          Swal.fire({
              icon: 'error',
              title: 'Gagal',
              html: ' {!! Session::get('error') !!}',
          })
      </script>
  @elseif(Session::has('info'))
      <script>
          Swal.fire({
              icon: 'info',
              title: 'Info',
              html: ' {!! Session::get('info') !!}',
          })
      </script>
  @elseif($errors->any())
      <script>
          Swal.fire({
              icon: 'error',
              title: 'Gagal',
              html: '@foreach($errors->all() as $error) {!! $error."<br>" !!}@endforeach',
          })
      </script>
  @endif
  
  @stack('js')

</body>

</html>