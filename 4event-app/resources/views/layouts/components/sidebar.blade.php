<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('dashboard') }}">
        <div class="sidebar-brand-icon">
            <img src="{{ asset('assets/img/logo-card-2.png') }}" width="100%">
        </div>                
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
    Home
    </div>

    <li class="nav-item {{ (request()->is('dashboard')) ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('dashboard') }}">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span>
    </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
    Event
    </div>

    <li class="nav-item @yield('event')">
    <a class="nav-link" href="{{ route('event.index') }}">
        <i class="fas fa-chart-bar"></i>
        <span>Data Event</span>
    </a>
    </li>

    @if (Auth::user()->role_id == 1)

    <li class="nav-item @yield('peserta')">
        <a class="nav-link" href="{">
            <i class="fas fa-chart-bar"></i>
            <span>Data Semua Peserta</span>
        </a>
    </li>
        
    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
    Master Data
    </div>

    <li class="nav-item @yield('kategori')">
    <a class="nav-link" href="{{ route('kategori.index') }}">
        <i class="fas fa-database"></i>
        <span>Kategori</span>
    </a>
    </li>
    
    <li class="nav-item @yield('jurusan')">
    <a class="nav-link" href="{{ route('jurusan.index') }}">
        <i class="fas fa-database"></i>
        <span>Jurusan</span>
    </a>
    </li>

    <li class="nav-item @yield('kelas')">
    <a class="nav-link" href="{{ route('kelas.index') }}">
        <i class="fas fa-database"></i>
        <span>Kelas</span>
    </a>
    </li>    
    
    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
    Management
    </div>

    <li class="nav-item @yield('user')">
    <a class="nav-link" href="{{ route('users.index') }}">
        <i class="fa fa-user"></i>
        <span>Users</span>
    </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
    Pesan
    </div>

    <li class="nav-item @yield('pesan')">
    <a class="nav-link" href="{{ route('inbox.index') }}">
        <i class="fa fa-envelope"></i>
        <span>Pesan Masuk</span>
    </a>
    </li>
    @endif
    
</ul>
<!-- End of Sidebar -->